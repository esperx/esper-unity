﻿using System;
using UnityEngine;

namespace Esper.Example
{
	[Serializable]
	public class GamePhase
	{
		[SerializeField] private int applyAtDistance;
		[SerializeField] private float speed;
		[SerializeField] private float jumpSpeed;
		[SerializeField] private float obstacleFrequency;
		[SerializeField] private float pyramidProbability;
		[SerializeField] private float rockProbability;

		public float ApplyAtDistance { get { return applyAtDistance; } }

		public float Speed { get { return speed; } }

		public float JumpSpeed { get { return jumpSpeed; } }

		public float ObstacleFrequency { get { return obstacleFrequency; } }

		public float PyramidProbability { get { return pyramidProbability; } }

		public float RockProbability { get { return rockProbability; } }
	}
}

