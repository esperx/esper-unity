﻿namespace Esper.Example
{
	public static class Scenes
	{
		public const string MainMenu = "WelcomeScreen";
		public const string Game = "GamePlayScreen";
	}
}