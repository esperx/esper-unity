﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Esper.Example
{
	[Serializable]
	public class Leaderboard
	{
		[SerializeField] private List<PlayerScore> scores;

		public List<PlayerScore> Scores
		{ 
			get
			{
				return scores;
			}
		}
	}
}