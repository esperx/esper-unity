using System.Collections.Generic;

namespace Esper.Example
{
	public class PhaseMapping
	{
		private Dictionary <string, GamePhase> phases;
		private string currentPhase;

		public void Initialise(List<GamePhase> phaseList)
		{
			phases = new Dictionary <string, GamePhase>();
			for (int i = 0; i < phaseList.Count; ++i)
			{
				phases["phase" + i] = phaseList[i];
			}
		}

		public bool CheckForNewPhase(int distance)
		{
			if(distance > 0 && distance <= 15 && currentPhase != "phase0")
			{
				currentPhase = "phase0";
				return true;
			}
			if(distance > 15 && distance <= 30 && currentPhase != "phase1")
			{
				currentPhase = "phase1";
				return true;
			}
			if(distance > 30 && distance <= 40 && currentPhase != "phase2")
			{
				currentPhase = "phase2";
				return true;
			}
			if(distance > 40 && distance <= 50 && currentPhase != "phase3")
			{
				currentPhase = "phase3";
				return true;
			}
			return false;
		}

		public GamePhase GetPhase()
		{
			return phases[currentPhase];
		}
	}
}
