﻿using System.Collections.Generic;
using UnityEngine;

namespace Esper.Example
{
	public class GameConfig : ScriptableObject
	{
		[SerializeField] private List<GamePhase> phases;

		public List<GamePhase> Phases
		{
			get
			{
				return phases;
			}
		}
	}
}