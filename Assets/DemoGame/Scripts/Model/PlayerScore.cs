﻿using System;
using UnityEngine;

namespace Esper.Example
{
	[Serializable]
	public class PlayerScore
	{
		[SerializeField] private string name;
		[SerializeField] private int score;

		public string Name
		{ 
			get
			{ 
				return name; 
			}
		}

		public int Score
		{ 
			get
			{ 
				return score; 
			}
		}
	}
}