﻿using Esper.Example;
using Esper.Extensions.Basic;
using Esper.FSM;

public class DemoGameRoot : BasicEsperRoot 
{
	private static DemoGameRoot instance;
	private FSMConfig config;
	
	public HighVolumeService HighVolumeService { get; private set; }

	public void StartUp(FSMConfig config)
	{
		this.config = config;
		instance = this;
		Initialise();	
	}

	public static DemoGameRoot Instance
	{
		get { return instance; }
	}

	protected override void PrepareApplication()
	{
		base.PrepareApplication();
		HighVolumeService = new HighVolumeService();
	}

	protected override void StartApplication()
	{
		base.StartApplication();
		fsmSignalFactory = new FSMSignalFactory(signalFactory);
		fsmController.Initialise(config, stateTransitionSignal, fsmSignalFactory);
	}

	protected override void PrepareCommands()
	{
		base.PrepareCommands();
		Signal<OnBaseLayerBootstrappingEnter>().MapCommand<BootstrapCommand>();
		Signal<TriggerSequenceCommandSignal>().MapCommand<DemoSequenceCommand>().RestrictExecutionsTo(5).ManualDispose();
		Signal<ChangeSceneSignal>().MapCommand<ChangeSceneCommand>();
		Signal<OnMainMenuRetrievingLeaderboardEnter>().MapCommand<RetrieveLeaderboardCommand>();
		Signal<OnMainMenuRetrievingCustomisationEnter>().MapCommand<RetrieveCustomisationCommand>();
		Signal<HighVolumeSignal>().MapCommand<HighVolumeCommand>();
	}
}
