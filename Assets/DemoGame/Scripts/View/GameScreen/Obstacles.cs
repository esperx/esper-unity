﻿using System.Collections.Generic;
using UnityEngine;

namespace Esper.Example
{
    public class Obstacles : MonoBehaviour
    {
        private readonly Vector3 inactivePosition = new Vector3(0f, 0f, -2f);
        private const float OffscreenZ = -2f;
        
        [SerializeField] private GameObject rockPrefab;
        [SerializeField] private GameObject pyramidPrefab;
        [Range(1, 30)] [SerializeField] private int pyramidPoolSize;
        [Range(1, 30)] [SerializeField] private int rockPoolSize;
        [SerializeField] private Transform generatedTerrain;
        [SerializeField] private float fieldDistance = 5f;
        [SerializeField] private int seed = 1;

        private Queue<Transform> pyramidPool;
        private Queue<Transform> rockPool;
        private List<Transform> activePyramids;
        private List<Transform> activeRocks;
        private Vector3 initialActivePosition = new Vector3(0f, 5f, 20f);
        private Vector3 moveTowardsPosition = new Vector3(0f, 0.6f, 0f);
        private Vector3 obstaclePosition;
        private Vector3 velocity;
        private Vector3 checkPosition;
        private GamePhase phase;
        private float lastGeneratedObjectTime = 1f;
        private int numberActiveRocks;
        private int numberActivePyramids;
        private float obstacleGradientY;
        private float obstacleGradientZ;

        public void Initialise()
        {
            Random.InitState(seed);
            activePyramids = new List<Transform>(pyramidPoolSize);
            activeRocks = new List<Transform>(rockPoolSize);
            obstacleGradientY = (initialActivePosition.y - moveTowardsPosition.y) / fieldDistance;
            obstacleGradientZ = (initialActivePosition.z - moveTowardsPosition.z) / fieldDistance;
            CreatePools();
        }

        public void SetGamePhase(GamePhase phase)
        {
            this.phase = phase;
        }

        public void Tick(float horizontalMovement)
        {
            velocity.x = phase.Speed * horizontalMovement * Time.deltaTime * 5;
            velocity.y = phase.Speed * obstacleGradientY * Time.deltaTime * 5;
            velocity.z = phase.Speed * obstacleGradientZ * Time.deltaTime * 5;
            GenerateNewObstacles();
            MoveObstacles(velocity.x, velocity.y, velocity.z);
            CheckForRepool();
        }

        public HitResult CheckForHit()
        {
            for (int i = 0; i < numberActivePyramids; ++i)
            {
                checkPosition = activePyramids[i].position;
                if ((checkPosition.x * checkPosition.x) + (checkPosition.z * checkPosition.z) < 0.3f)
                {
                    return HitResult.Pyramid;
                }
            }
            for (int i = 0; i < numberActiveRocks; ++i)
            {
                checkPosition = activeRocks[i].position;
                if ((checkPosition.x * checkPosition.x) + (checkPosition.z * checkPosition.z) < 0.3f)
                {
                    return HitResult.Rock;
                }
            }
            return HitResult.None;
        }

        private void CreatePools()
        {
            pyramidPool = new Queue<Transform>(pyramidPoolSize);
            for (int i = 0; i < pyramidPoolSize; ++i)
            {
                var pyramidTransform = Instantiate(pyramidPrefab).transform;
                pyramidTransform.localPosition = inactivePosition;
                pyramidTransform.parent = generatedTerrain;
                pyramidTransform.name = "Pyramid";
                pyramidPool.Enqueue(pyramidTransform);
            }
            rockPool = new Queue<Transform>(rockPoolSize);
            for (int i = 0; i < rockPoolSize; ++i)
            {
                var rockTransform = Instantiate(rockPrefab).transform;
                rockTransform.localPosition = inactivePosition;
                rockTransform.parent = generatedTerrain;
                rockTransform.name = "Rock";
                rockPool.Enqueue(rockTransform);
            }
        }

        private void GenerateNewObstacles()
        {
            if (!(Time.realtimeSinceStartup - lastGeneratedObjectTime > phase.ObstacleFrequency)) return;
            float rand = Random.value;
            if (rand <= phase.PyramidProbability)
            {
                if (numberActivePyramids < pyramidPoolSize)
                {
                    var pyramid = pyramidPool.Dequeue();
                    pyramid.gameObject.SetActive(true);
                    initialActivePosition.x = Random.value * 17;
                    pyramid.localPosition = initialActivePosition;
                    activePyramids.Add(pyramid);
                    numberActivePyramids++;
                    lastGeneratedObjectTime = Time.realtimeSinceStartup;
                }
            }
            if (rand > phase.RockProbability || numberActiveRocks >= rockPoolSize) return;
            var rock = rockPool.Dequeue();
            rock.gameObject.SetActive(true);
            initialActivePosition.x = Random.value * 17;
            rock.localPosition = initialActivePosition;
            activeRocks.Add(rock);
            numberActiveRocks++;
            lastGeneratedObjectTime = Time.realtimeSinceStartup;
        }

        private void MoveObstacles(float velocityX, float velocityY, float velocityZ)
        {
            for (int i = numberActiveRocks - 1; i >= 0; --i)
            {
                obstaclePosition.x = activeRocks[i].localPosition.x - velocityX;
                obstaclePosition.y = activeRocks[i].localPosition.y - velocityY;
                obstaclePosition.z = activeRocks[i].localPosition.z - velocityZ;
                activeRocks[i].localPosition = obstaclePosition;
            }
            for (int i = numberActivePyramids - 1; i >= 0; --i)
            {
                obstaclePosition.x = activePyramids[i].localPosition.x - velocityX;
                obstaclePosition.y = activePyramids[i].localPosition.y - velocityY;
                obstaclePosition.z = activePyramids[i].localPosition.z - velocityZ;
                activePyramids[i].localPosition = obstaclePosition;
            }
        }

        private void CheckForRepool()
        {
            for (int i = numberActiveRocks - 1; i >= 0; --i)
            {
                if (activeRocks[i].localPosition.z > OffscreenZ) continue;
                var rock = activeRocks[i];
                rock.gameObject.SetActive(false);
                activeRocks.RemoveAt(i);
                rockPool.Enqueue(rock);
                numberActiveRocks--;
            }
            for (int i = numberActivePyramids - 1; i >= 0; --i)
            {
                if (activePyramids[i].localPosition.z > OffscreenZ) continue;

                var pyramid = activePyramids[i];
                pyramid.gameObject.SetActive(false);
                activePyramids.RemoveAt(i);
                pyramidPool.Enqueue(pyramid);
                numberActivePyramids--;
            }
        }
    }
}