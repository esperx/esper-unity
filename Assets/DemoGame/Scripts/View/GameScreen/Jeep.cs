﻿using UnityEngine;

namespace Esper.Example
{
	public class Jeep : MonoBehaviour
	{
		[SerializeField] private AnimationCurve jumpCurve;
		[SerializeField] private GameObject jeepContainer;
		[SerializeField] private GameObject explosion;
		[SerializeField] private Transform jeepTop;
		[SerializeField] private Transform jeepRotationTransform;
		[SerializeField] private float movingLeftPosition = -0.1f;
		[SerializeField] private float movingCentrePosition = 0f;
		[SerializeField] private float movingRightPosition = 0.1f;
		[SerializeField] private float maxJumpHeight = 1.2f;
		private GamePhase phase;
		private Transform jeepTransform;
		private Vector3 currentJeepPosition;
		private Vector3 currentJeepRotation;
		private Vector3 jeepTopPosition;
		private float jumpTime = 1f;
		private float currentJumpTime = 1f;
		private bool isActive = true;
		private bool isJumping;

		private void Awake()
		{
			explosion.SetActive(false);
			jeepTransform = transform;
			jeepTopPosition = jeepTop.localPosition;
		}

		public void SetGamePhase(GamePhase phase)
		{
			this.phase = phase;
		}

		public void SetNewDirection(Direction direction)
		{
			switch (direction)
			{
				case Direction.Left:
					jeepTopPosition.x = movingLeftPosition;
					break;
				case Direction.Right:
					jeepTopPosition.x = movingRightPosition;
					break;
				default:
					jeepTopPosition.x = movingCentrePosition;
					break;
			}
			jeepTop.localPosition = jeepTopPosition;
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		public void EnableControls()
		{
			isActive = true;
		}

		public void Tick()
		{
			if (!isActive) return;
			if (isJumping)
			{
				currentJumpTime += Time.deltaTime;
				float currentJumpCompleted = currentJumpTime / jumpTime;
				currentJeepPosition.y = jumpCurve.Evaluate(currentJumpCompleted) * maxJumpHeight;
				jeepTransform.localPosition = currentJeepPosition;
				currentJeepRotation.z = Mathf.Lerp(0, 360, currentJumpCompleted);
				jeepRotationTransform.localRotation = Quaternion.Euler(currentJeepRotation);
				isJumping = currentJumpCompleted < 1;
			}
			else
			{
				jumpTime = phase.JumpSpeed;
			}
		}

		public void Jump()
		{
			if (isJumping) return;
			currentJumpTime = 0f;
			isJumping = true;
		}

		public bool IsJumping()
		{
			return isJumping;
		}

		public void Dead()
		{
			isActive = false;
			jeepContainer.SetActive(false);
			explosion.SetActive(true);
		}
	}
}