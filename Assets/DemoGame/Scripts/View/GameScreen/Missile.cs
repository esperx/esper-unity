﻿using UnityEngine;

namespace Esper.Example
{
	public class Missile : MonoBehaviour
	{
		private const float MaxFireTime = 0.5f;
		[SerializeField] private Transform evilPyramid;
		[SerializeField] private GameObject explosion;
		private Transform missileTransform;
		private Vector3 endPosition;
		private float startTime;
		private bool isComplete;

		private void Awake()
		{
			missileTransform = transform;
			explosion.SetActive(false);
			endPosition = evilPyramid.localPosition;
		}

		public void Enable()
		{
			startTime = Time.realtimeSinceStartup;
			gameObject.SetActive(true);
		}

		public void Tick()
		{
			var percentComplete = (Time.realtimeSinceStartup - startTime) / MaxFireTime;
			missileTransform.localPosition = Vector3.Lerp(missileTransform.localPosition, endPosition, percentComplete);
			if (!(percentComplete >= 1)) return;
			explosion.SetActive(true);
			isComplete = true;
		}

		public bool Complete()
		{
			return isComplete;
		}
	}
}