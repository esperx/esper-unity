﻿using System.Collections;
using System.Text;
using Esper.FSM;
using UnityEngine;
using UnityEngine.UI;

namespace Esper.Example
{
    public class GameRoot : MonoBehaviour
    {
        private readonly StringBuilder stringBuilder = new StringBuilder(17, 17);
        private const string HorizontalAxis = "Horizontal";
        private const int MaxDistance = 50;

        [SerializeField] private GameConfig config;
        [SerializeField] private GameObject skull;
        [SerializeField] private GameObject gameWon;
        [SerializeField] private Missile missile;
        [SerializeField] private Obstacles obstacles;
        [SerializeField] private Transform generatedTerrain;
        [SerializeField] private Jeep jeep;
        [SerializeField] private CanvasGroup fadePanel;
        [SerializeField] private Text distanceToGo;
        private PhaseMapping phaseMapper;
        private float horizontalMovement;
        private float lastDistanceUpdate;
        private float speed;
        private bool isPlaying;
        private bool isPlayerDead;
        private int roundedDistance;
        private int lastPrintedDistance;

        private void Awake()
        {
            phaseMapper = new PhaseMapping();
            phaseMapper.Initialise(config.Phases);
            speed = 1;
            fadePanel.alpha = 1;
            roundedDistance = MaxDistance;
            obstacles.Initialise();
            CheckCurrentGamePhase();
        }

        private void CheckCurrentGamePhase()
        {
            if (!phaseMapper.CheckForNewPhase(roundedDistance)) return;
            var phase = phaseMapper.GetPhase();
            speed = phase.Speed;
            jeep.SetGamePhase(phase);
            obstacles.SetGamePhase(phase);
        }

        private void Start()
        {
            StartCoroutine(FadeIn());
        }

        private IEnumerator FadeIn()
        {
            while (fadePanel.alpha > 0)
            {
                fadePanel.alpha -= Time.deltaTime * 2f;
                yield return null;
            }
            StartGame();
        }

        private void OnLifeLost()
        {
            isPlaying = false;
            isPlayerDead = true;
            ShowGameOverScreen();
        }

        private void StartGame()
        {
            jeep.EnableControls();
            isPlaying = true;
        }

        private void Update()
        {
            if (!isPlaying) return;
            SetDistance();
            if (roundedDistance <= 0)
            {
                isPlaying = false;
                roundedDistance = 0;
                UpdateDistanceToGo();
                HideAll();
                StartCoroutine(FireMissile());
            }
            else
            {
                CheckCurrentGamePhase();
                GetDirection();
                UpdateDistanceToGo();
                obstacles.Tick(horizontalMovement);
                jeep.Tick();
            }
        }

        private void FixedUpdate()
        {
            if (!isPlaying || jeep.IsJumping()) return;

            var result = obstacles.CheckForHit();
            switch (result)
            {
                case HitResult.Rock:
                    jeep.Jump();
                    break;
                case HitResult.Pyramid:
                    jeep.Dead();
                    OnLifeLost();
                    break;
            }
        }

        private void HideAll()
        {
            jeep.Hide();
            generatedTerrain.gameObject.SetActive(false);
        }

        private void GetDirection()
        {
            horizontalMovement = Input.GetAxis(HorizontalAxis);
            if (horizontalMovement > 0)
            {
                jeep.SetNewDirection(Direction.Right);
            }
            else if (horizontalMovement < 0)
            {
                jeep.SetNewDirection(Direction.Left);
            }
            else
            {
                jeep.SetNewDirection(Direction.Straight);
            }
        }

        private void SetDistance()
        {
            var timeForUnitDistance = 1f / speed;
            if (Time.realtimeSinceStartup - lastDistanceUpdate < timeForUnitDistance) return;
            lastDistanceUpdate = Time.realtimeSinceStartup;
            roundedDistance--;
        }

        private IEnumerator FireMissile()
        {
            missile.Enable();
            while (!missile.Complete())
            {
                missile.Tick();
                yield return null;
            }
            yield return new WaitForSeconds(2);
            StartCoroutine(ShowGameWonScreen());
        }

        private IEnumerator ShowGameWonScreen()
        {
            gameWon.SetActive(true);
            yield return new WaitForSeconds(3);
            ShowGameOverScreen();
        }

        private void UpdateDistanceToGo()
        {
            if (roundedDistance == lastPrintedDistance) return;

            stringBuilder.Length = 0;
            stringBuilder.Concat(roundedDistance);
            distanceToGo.text = stringBuilder.ToString();
            lastPrintedDistance = roundedDistance;
        }

        private void ShowGameOverScreen()
        {
            ReturnToMainMenu();
        }

        private IEnumerator FadeOut()
        {
            while (fadePanel.alpha < 1)
            {
                fadePanel.alpha += Time.deltaTime * 2f;
                yield return null;
            }
            if (isPlayerDead)
            {
                skull.SetActive(true);
                yield return new WaitForSeconds(2);
            }
            OnFadeOutComplete();
        }

        private void OnFadeOutComplete()
        {
            DemoGameRoot.Instance.Signal<ChangeSceneSignal>().Fire(Scenes.MainMenu, FSMTransitions.BaseLayerMainMenuTopLevelMenu);
        }

        private void ReturnToMainMenu()
        {
            StartCoroutine(FadeOut());
        }
    }
}