﻿using UnityEngine;

namespace Esper.Example
{
	public class MainMenuView : MonoBehaviour 
	{
		private readonly Vector3 OffScreenPosition = new Vector3(-800, 0, 0);
		private readonly Vector3 VisiblePosition = Vector3.zero;

		public void Hide()
		{
			transform.localPosition = OffScreenPosition;
			Pause();
		}

		public void Show()
		{
			transform.localPosition = VisiblePosition;
			Activate();
		}

		private void Pause()
		{
		}

		private void Activate()
		{
		}
	}
}