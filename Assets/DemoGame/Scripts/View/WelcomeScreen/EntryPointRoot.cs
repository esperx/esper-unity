﻿using System.Collections;
using Esper.FSM;
using UnityEngine;
using UnityEngine.UI;

namespace Esper.Example
{
    public class EntryPointRoot : MonoBehaviour
    {
        private readonly SeqPassthrough passThrough = new SeqPassthrough();
        [SerializeField] private FSMConfig config;
        [SerializeField] private Button mainMenuBtn;
        [SerializeField] private Button leaderboardBtn;
        [SerializeField] private Button customisationBtn;
        [SerializeField] private Button playBtn;
        [SerializeField] private CanvasGroup fadePanel;
        [SerializeField] private LeaderboardView leaderboard;
        [SerializeField] private CustomisationView customisation;
        [SerializeField] private MainMenuView mainMenu;

        private DemoGameRoot root;
        private TriggerSequenceCommandSignal seqSignal;
        private HighVolumeSignal highVolumeSignal;

        private bool isInteractable;

        private void Awake()
        {
            Application.targetFrameRate = 60;
            fadePanel.alpha = 1;
            InitialiseEsper();

        }

        private void InitialiseEsper()
        {
            if (root != null) return;

            if (DemoGameRoot.Instance != null)
            {
                root = DemoGameRoot.Instance;
            }
            else
            {
                var go = new GameObject("EsperRoot");
                DontDestroyOnLoad(go);
                root = go.AddComponent<DemoGameRoot>();
                root.StartUp(config);
            }
            
            seqSignal = root.Signal<TriggerSequenceCommandSignal>();
            highVolumeSignal = root.Signal<HighVolumeSignal>();
            root.Signal<OnMainMenuTopLevelMenuEnter>().Subscribe(ShowMainMenu);
            root.Signal<OnMainMenuLeaderboardEnter>().Subscribe(ShowLeaderboard);
            root.Signal<OnMainMenuPlayerCustomisationEnter>().Subscribe(ShowCustomisation);
        }

        private void Update()
        {
            seqSignal.Fire(passThrough);
            highVolumeSignal.Fire();
        }

        private void Start()
        {
            
            StartCoroutine(FadeIn());
        }

        private IEnumerator FadeIn()
        {
            while (fadePanel.alpha > 0)
            {
                fadePanel.alpha -= Time.deltaTime * 2f;
                yield return null;
            }
            OnFadeInComplete();
        }

        private void OnFadeInComplete()
        {
            isInteractable = true;
        }

        private void OnDestroy()
        {
            root.Signal<OnMainMenuTopLevelMenuEnter>().Unsubscribe(ShowMainMenu);
            root.Signal<OnMainMenuLeaderboardEnter>().Unsubscribe(ShowLeaderboard);
            root.Signal<OnMainMenuPlayerCustomisationEnter>().Unsubscribe(ShowCustomisation);
            mainMenuBtn.onClick = null;
            leaderboardBtn = null;
            customisationBtn = null;
            playBtn = null;
        }

        public void OnMainMenuRequest()
        {
            if (!isInteractable) return;

            DisableButtons();
            root.TriggerState(FSMTransitions.BaseLayerMainMenuTopLevelMenu);
        }

        public void OnLeaderboardRequest()
        {
            if (!isInteractable) return;

            DisableButtons();
            root.TriggerState(FSMTransitions.BaseLayerMainMenuRetrievingLeaderboard);
        }

        public void OnCustomisationRequest()
        {
            if (!isInteractable) return;

            DisableButtons();
            root.TriggerState(FSMTransitions.BaseLayerMainMenuRetrievingCustomisation);
        }

        public void OnGameRequest()
        {
            if (!isInteractable) return;

            isInteractable = false;
            StartCoroutine(FadeOut());
        }

        private IEnumerator FadeOut()
        {
            while (fadePanel.alpha < 1)
            {
                fadePanel.alpha += Time.deltaTime * 2f;
                yield return null;
            }
            OnFadeOutComplete();
        }

        private void OnFadeOutComplete()
        {
            root.Signal<ChangeSceneSignal>().Fire(Scenes.Game, FSMTransitions.BaseLayerGamePlay);
        }

        private void DisableButtons()
        {
            mainMenuBtn.interactable = false;
            leaderboardBtn.interactable = false;
            customisationBtn.interactable = false;
            playBtn.interactable = false;
        }

        public void ShowMainMenu(object passthrough = null)
        {
            leaderboard.Hide();
            customisation.Hide();
            mainMenu.Show();
            mainMenuBtn.interactable = false;
            leaderboardBtn.interactable = true;
            customisationBtn.interactable = true;
            playBtn.interactable = true;
        }

        public void ShowLeaderboard(object passthrough = null)
        {
            var data = (Leaderboard) passthrough;
            leaderboard.SetData(data);
            mainMenu.Hide();
            customisation.Hide();
            leaderboard.Show();
            mainMenuBtn.interactable = true;
            leaderboardBtn.interactable = false;
            customisationBtn.interactable = true;
            playBtn.interactable = true;
        }

        public void ShowCustomisation(object passthrough = null)
        {
            var data = (CustomisationData) passthrough;
            customisation.SetData(data);
            mainMenu.Hide();
            leaderboard.Hide();
            customisation.Show();
            mainMenuBtn.interactable = true;
            leaderboardBtn.interactable = true;
            customisationBtn.interactable = false;
            playBtn.interactable = true;
        }
    }
}