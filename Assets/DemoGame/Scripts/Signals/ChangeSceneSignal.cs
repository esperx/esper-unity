﻿using Esper.Signals;

namespace Esper.Example
{
	public class ChangeSceneSignal : Signal<string, string>
	{
	}
}