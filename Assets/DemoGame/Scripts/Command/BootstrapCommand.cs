﻿using Esper.Commands;
using Esper.FSM;

namespace Esper.Example
{
	public class BootstrapCommand : Command<object>
	{
		public override void Execute(object arg1)
		{
			DemoGameRoot.Instance.TriggerState(FSMTransitions.BaseLayerMainMenuTopLevelMenu);
		}
	}
}