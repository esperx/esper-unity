﻿using Esper.Commands;

namespace Esper.Example
{
	public class HighVolumeCommand : Command
	{
		public override void Execute()
		{
			DemoGameRoot.Instance.HighVolumeService.Send();
		}
	}
}