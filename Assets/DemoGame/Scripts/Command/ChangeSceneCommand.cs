﻿using System.Collections;
using Esper.Commands;
using UnityEngine.SceneManagement;

namespace Esper.Example
{
	public sealed class ChangeSceneCommand : AsyncCommand<string, string>
	{
		private string requestedScene;
		private string stateId;

		public override void Execute(string sceneName, string stateId)
		{
			this.requestedScene = sceneName;
			this.stateId = stateId;
			LoadScene();
		}

		private void LoadScene()
		{
			if (SceneManager.GetActiveScene().name != requestedScene)
			{
				DemoGameRoot.Instance.CoroutineService.Process(DoLoad());
			}
			else
			{
				SceneLoadCompleted();
			}
		}

		private IEnumerator DoLoad()
		{
			var sceneLoader = SceneManager.LoadSceneAsync(requestedScene);

			while (!sceneLoader.isDone)
			{
				yield return null;
			}
			SceneLoadCompleted();
		}

		private void SceneLoadCompleted()
		{
			if (!string.IsNullOrEmpty(stateId))
			{
				DemoGameRoot.Instance.TriggerState(stateId);
			}
			CommandComplete();
		}
	}
}