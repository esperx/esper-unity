﻿using Esper.Commands;
using Esper.FSM;
using UnityEngine;

namespace Esper.Example
{
	public class RetrieveLeaderboardCommand : Command<object>
	{
		public override void Execute(object passthroughData)
		{
			Debug.Log("RetrieveLeaderboardCommand ");
			var leaderboardJson = Resources.Load<TextAsset>("leaderboard");
			var leaderboard = JsonUtility.FromJson<Leaderboard>(leaderboardJson.text);
			DemoGameRoot.Instance.TriggerState(FSMTransitions.BaseLayerMainMenuLeaderboard, leaderboard);
		}
	}
}