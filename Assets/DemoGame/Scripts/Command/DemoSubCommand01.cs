﻿using Esper.Commands;

namespace Esper.Example
{
	public class DemoSubCommand01 : Command<SeqPassthrough>
	{
		public override void Execute(SeqPassthrough arg1 = null)
		{
			arg1.Field1 = 10;
		}
	}
}

