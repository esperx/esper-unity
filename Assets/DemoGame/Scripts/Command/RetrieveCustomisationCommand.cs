﻿using Esper.Commands;
using Esper.FSM;
using UnityEngine;

namespace Esper.Example
{
	public class RetrieveCustomisationCommand : Command<object>
	{
		public override void Execute(object passthroughData)
		{
			var customisationJson = Resources.Load<TextAsset>("customisation");
			var customisation = JsonUtility.FromJson<CustomisationData>(customisationJson.text);
			DemoGameRoot.Instance.TriggerState(FSMTransitions.BaseLayerMainMenuPlayerCustomisation, customisation);
		}
	}
}