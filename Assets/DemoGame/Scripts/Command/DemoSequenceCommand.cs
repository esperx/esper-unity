﻿using Esper.Commands;

namespace Esper.Example
{
	public class DemoSequenceCommand : SequenceCommand<SeqPassthrough>
	{
		protected override void QueueCommands()
		{
			base.QueueCommands();
			AddCommand<DemoSubCommand01>();
			AddCommand<DemoSubCommand02>();
			AddCommand<DemoSubCommand03>();
		}
	}
}