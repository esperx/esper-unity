namespace Esper.FSMController
{
	public static class FSMTransitions
	{
		public const string BaseLayerBootstrapping = "Base Layer/Bootstrapping";
		public const string BaseLayerGamePlay = "Base Layer/GamePlay";
		public const string BaseLayerMainMenuTopLevelMenu = "Base Layer/MainMenu/TopLevelMenu";
		public const string BaseLayerMainMenuLeaderboard = "Base Layer/MainMenu/Leaderboard";
		public const string BaseLayerMainMenuRetrievingLeaderboard = "Base Layer/MainMenu/RetrievingLeaderboard";
		public const string BaseLayerMainMenuPlayerCustomisation = "Base Layer/MainMenu/PlayerCustomisation";
		public const string BaseLayerMainMenuRetrievingCustomisation = "Base Layer/MainMenu/RetrievingCustomisation";
	}
}
