using Esper.FSM;
using Esper.Signals;

namespace Esper.FSMController
{
	public class FSMSignalFactory : IFSMSignalFactory
	{
		private ISignalFactory signalFactory;
		private OnBaseLayerBootstrappingEnter onBaseLayerBootstrappingEnter;
		private OnBaseLayerGamePlayEnter onBaseLayerGamePlayEnter;
		private OnMainMenuTopLevelMenuEnter onMainMenuTopLevelMenuEnter;
		private OnMainMenuLeaderboardEnter onMainMenuLeaderboardEnter;
		private OnMainMenuRetrievingLeaderboardEnter onMainMenuRetrievingLeaderboardEnter;
		private OnMainMenuPlayerCustomisationEnter onMainMenuPlayerCustomisationEnter;
		private OnMainMenuRetrievingCustomisationEnter onMainMenuRetrievingCustomisationEnter;

		public FSMSignalFactory(ISignalFactory signalFactory)
		{
			this.signalFactory = signalFactory;
		}

		public void PrewarmSignals()
		{
			if (onBaseLayerBootstrappingEnter == null) onBaseLayerBootstrappingEnter = signalFactory.GetSignal<OnBaseLayerBootstrappingEnter>();
			if (onBaseLayerGamePlayEnter == null) onBaseLayerGamePlayEnter = signalFactory.GetSignal<OnBaseLayerGamePlayEnter>();
			if (onMainMenuTopLevelMenuEnter == null) onMainMenuTopLevelMenuEnter = signalFactory.GetSignal<OnMainMenuTopLevelMenuEnter>();
			if (onMainMenuLeaderboardEnter == null) onMainMenuLeaderboardEnter = signalFactory.GetSignal<OnMainMenuLeaderboardEnter>();
			if (onMainMenuRetrievingLeaderboardEnter == null) onMainMenuRetrievingLeaderboardEnter = signalFactory.GetSignal<OnMainMenuRetrievingLeaderboardEnter>();
			if (onMainMenuPlayerCustomisationEnter == null) onMainMenuPlayerCustomisationEnter = signalFactory.GetSignal<OnMainMenuPlayerCustomisationEnter>();
			if (onMainMenuRetrievingCustomisationEnter == null) onMainMenuRetrievingCustomisationEnter = signalFactory.GetSignal<OnMainMenuRetrievingCustomisationEnter>();
		}
		public StateSignal GetSignal(string signalId)
		{
			switch(signalId)
			{
				case"OnBaseLayerBootstrappingEnter": return onBaseLayerBootstrappingEnter ?? (onBaseLayerBootstrappingEnter = signalFactory.GetSignal<OnBaseLayerBootstrappingEnter>());
				case"OnBaseLayerGamePlayEnter": return onBaseLayerGamePlayEnter ?? (onBaseLayerGamePlayEnter = signalFactory.GetSignal<OnBaseLayerGamePlayEnter>());
				case"OnMainMenuTopLevelMenuEnter": return onMainMenuTopLevelMenuEnter ?? (onMainMenuTopLevelMenuEnter = signalFactory.GetSignal<OnMainMenuTopLevelMenuEnter>());
				case"OnMainMenuLeaderboardEnter": return onMainMenuLeaderboardEnter ?? (onMainMenuLeaderboardEnter = signalFactory.GetSignal<OnMainMenuLeaderboardEnter>());
				case"OnMainMenuRetrievingLeaderboardEnter": return onMainMenuRetrievingLeaderboardEnter ?? (onMainMenuRetrievingLeaderboardEnter = signalFactory.GetSignal<OnMainMenuRetrievingLeaderboardEnter>());
				case"OnMainMenuPlayerCustomisationEnter": return onMainMenuPlayerCustomisationEnter ?? (onMainMenuPlayerCustomisationEnter = signalFactory.GetSignal<OnMainMenuPlayerCustomisationEnter>());
				case"OnMainMenuRetrievingCustomisationEnter": return onMainMenuRetrievingCustomisationEnter ?? (onMainMenuRetrievingCustomisationEnter = signalFactory.GetSignal<OnMainMenuRetrievingCustomisationEnter>());
			}
			return null;
		}
		public void Dispose()
		{
			onBaseLayerBootstrappingEnter.Dispose();
			onBaseLayerGamePlayEnter.Dispose();
			onMainMenuTopLevelMenuEnter.Dispose();
			onMainMenuLeaderboardEnter.Dispose();
			onMainMenuRetrievingLeaderboardEnter.Dispose();
			onMainMenuPlayerCustomisationEnter.Dispose();
			onMainMenuRetrievingCustomisationEnter.Dispose();
		}
	}
}
