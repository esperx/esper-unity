﻿using System.Collections;
using UnityEngine;

namespace Esper.Services
{
	public class CoroutineService : MonoBehaviour, ICoroutineService
	{
		public Coroutine Process(IEnumerator process)
		{
			return StartCoroutine(process);
		}

		public void EndCoroutine(Coroutine enumerable)
		{
			StopCoroutine(enumerable);
		}
	}
}