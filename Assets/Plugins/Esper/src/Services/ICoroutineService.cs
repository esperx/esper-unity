﻿using System.Collections;
using UnityEngine;

namespace Esper.Services
{
	public interface ICoroutineService
	{
		Coroutine Process(IEnumerator process);

		void EndCoroutine(Coroutine enumerable);
	}
}