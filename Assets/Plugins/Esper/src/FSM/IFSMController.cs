﻿namespace Esper.FSM
{
    public interface IFSMController
    {
        void Initialise(FSMConfig config, StateTransitionSignal stateTransitionSignal, IFSMSignalFactory signalFactory);
        string CurrentState { get; }
        void Dispose();
    }
}