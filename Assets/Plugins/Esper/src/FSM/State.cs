﻿using System.Collections.Generic;

namespace Esper.FSM
{
	public sealed class State
	{
		public readonly string Id;
		public readonly string Name;
		public readonly string Fullpath;
		public readonly HashSet<string> Transitions;
		public readonly StateSignal OnStateEnter;
		public readonly StateSignal OnStateExit;

		public State(string id, string name, string fullPath, HashSet<string> transitions, StateSignal onStateEnter, StateSignal onStateExit)
		{
			Id = id;
			Name = name;
			Fullpath = fullPath;
			Transitions = transitions;
			OnStateEnter = onStateEnter;
			OnStateExit = onStateExit;
		}

		override public string ToString()
		{
			return string.Format("State {0} :: {1}", Id, Name);
		}
	}
}