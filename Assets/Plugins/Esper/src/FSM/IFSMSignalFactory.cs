﻿namespace Esper.FSM
{
	public interface IFSMSignalFactory
	{
		StateSignal GetSignal(string signalId);

		void Dispose();
	}
}