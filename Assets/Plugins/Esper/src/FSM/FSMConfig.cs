﻿using System.Collections.Generic;
using UnityEngine;

namespace Esper.FSM
{
	public class FSMConfig : ScriptableObject
	{
		[SerializeField] private FSMData data;
		[SerializeField] private List<StateSO> stateList;
		[SerializeField] private string defaultStateId;

		private Dictionary<string, StateSO> states;

		public List<StateSO> StateList
		{ 
			get
			{ 
				return stateList; 
			} 

			set
			{ 
				stateList = value; 
			} 
		}

		public string DefaultStateId
		{ 
			get
			{ 
				return defaultStateId; 
			} 

			set
			{ 
				defaultStateId = value; 
			}  
		}

		public Dictionary<string, StateSO> States
		{ 
			get
			{
				if (stateList == null) return null;
				states = new Dictionary<string, StateSO>();
				for (var i = 0; i < stateList.Count; ++i)
				{
					var state = stateList[i];
					states.Add(state.Id, state);
				}
				return states;
			}
		}
	}
}