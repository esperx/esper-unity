﻿using UnityEngine;

namespace Esper.FSM
{
	public class FSMBehaviour : StateMachineBehaviour
	{
		[SerializeField] private bool onStateEnter;
		[SerializeField] private bool onStateExit;

		public bool AddStateEnter 
		{ 
			get 
			{ 
				return onStateEnter; 
			}

			set 
			{ 
				onStateEnter = value; 
			}  
		}

		public bool AddStateExit 
		{ 
			get 
			{ 
				return onStateExit; 
			}

			set 
			{ 
				onStateExit = value; 
			}   
		}
	}
}