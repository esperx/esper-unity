﻿using Esper.Signals;

namespace Esper.FSM
{
	public class StateTransitionSignal : Signal<string, object, bool>
	{
	}
}