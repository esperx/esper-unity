﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Esper.FSM
{
	[Serializable]
	public class StateSO
	{
		[SerializeField] private string id;
		[SerializeField] private string name;
		[SerializeField] private string fullpath;
		[SerializeField] private List<string> transitions;
		[SerializeField] private string onStateEnter;
		[SerializeField] private string onStateExit;
		[SerializeField] private string type;
		[SerializeField] private Vector3 position;

		public string Id
		{ 
			get
			{ 
				return id; 
			}

			set
			{ 
				id = value; 
			} 
		}

		public string Name
		{ 
			get
			{ 
				return name; 
			}

			set
			{ 
				name = value; 
			} 
		}

		public string FullPath
		{ 
			get
			{ 
				return fullpath; 
			}

			set
			{ 
				fullpath = value; 
			} 
		}

		public List<string> Transitions
		{ 
			get
			{ 
				return transitions; 
			}

			set
			{ 
				transitions = value; 
			} 
		}

		public string OnStateEnter
		{ 
			get
			{ 
				return onStateEnter; 
			}

			set
			{ 
				onStateEnter = value; 
			} 
		}

		public string OnStateExit
		{ 
			get
			{ 
				return onStateExit; 
			}

			set
			{ 
				onStateExit = value; 
			} 
		}

		public Vector3 Position
		{ 
			get
			{ 
				return position; 
			}

			set
			{ 
				position = value; 
			} 
		}

		public string Type
		{ 
			get
			{ 
				return type; 
			}

			set
			{ 
				type = value; 
			} 
		}
	}
}