﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Esper.FSM
{
	public class FSMController : IFSMController
	{
		private StateTransitionSignal transitionSignal;
		private IFSMSignalFactory signalFactory;
		private Dictionary<string, State> states;
		private State currentState;
		private string defaultState;

		public void Initialise(FSMConfig config, StateTransitionSignal stateTransitionSignal, IFSMSignalFactory signalFactory)
		{
			transitionSignal = stateTransitionSignal;
			this.signalFactory = signalFactory;
			states = new Dictionary<string, State>();
			transitionSignal.Subscribe(OnStateTransitionRequest);
			for (var i = 0; i < config.StateList.Count; ++i)
			{
				var stateSO = config.StateList[i];

				var state = new State(stateSO.Id, stateSO.Name, stateSO.FullPath, ConvertTransitions(stateSO.Transitions), ConvertEvent(stateSO.OnStateEnter), ConvertEvent(stateSO.OnStateExit));
				states.Add(state.Id, state);
			}

			defaultState = config.DefaultStateId;

			if (!string.IsNullOrEmpty(defaultState))
			{
				TransitionTo(states[defaultState], null);
			}
		}
		
		public string CurrentState
		{
			get
			{
				return currentState.Fullpath;
			}
		}

		private	HashSet<string> ConvertTransitions(List<string> transitions)
		{
			var transitionSet = new HashSet<string>();
			
			if (transitions.Count <= 0) return transitionSet;
			
			for (var i = 0; i < transitions.Count; ++i)
			{
				transitionSet.Add(transitions[i]);
			}
			
			return transitionSet;
		}

		private	StateSignal ConvertEvent(string eventName)
		{
			return signalFactory.GetSignal(eventName);
		}

		private void OnStateTransitionRequest(string requestedState, object data, bool immediateDispatch)
		{
			lock (states)
			{
				if (!currentState.Transitions.Contains(requestedState)) return;
				var newState = states[requestedState];
				TransitionTo(newState, data, immediateDispatch);
			}
		}

		private void TransitionTo(State state, object passThroughData, bool forceImmediateDispatch = false)
		{
			if (currentState != null)
			{
				if (currentState.OnStateExit != null)
				{
					var exitMsg = currentState.OnStateExit;

					if (forceImmediateDispatch)
					{
						exitMsg.FireImmediate(passThroughData);
					}
					else
					{
						exitMsg.Fire(passThroughData);
					}
				}
			}
			currentState = state;

			if (currentState.OnStateEnter == null) return;
			
			var enterMsg = currentState.OnStateEnter;
			
			if (forceImmediateDispatch)
			{
				enterMsg.FireImmediate(passThroughData);
			}
			else
			{
				enterMsg.Fire(passThroughData);
			}
		}

		public void Dispose()
		{
			transitionSignal.Unsubscribe(OnStateTransitionRequest);
		}
	}
}