﻿using Esper.FSM;

namespace Esper.Signals
{
	public abstract class BaseSignal : ISignal
	{
		internal abstract void ScheduledFire();

		public abstract void Dispose();

		public abstract void Initialise(ISignalScheduler scheduler, ICommandPoolFactory commandPoolFactory);
	}
}