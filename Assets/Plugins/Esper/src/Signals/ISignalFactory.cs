﻿namespace Esper.Signals
{
    public interface ISignalFactory
    {
        S GetSignal<S>() where S : ISignal, new();
    }
}