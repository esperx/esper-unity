﻿using System;
using System.Collections.Generic;
using Esper.Commands;
using Esper.FSM;

namespace Esper.Signals
{
    public class Signal : BaseSignal
    {
        private ICommandPoolFactory commandPoolFactory;
        private Action signalAction;
        private ISignalScheduler scheduler;
        private List<Command.Pool> commands;
        private Queue<Command.Pool> disposeList;
        private bool isSchedulingEnabled;

        protected virtual int MaxCommands
        {
            get { return 10; }
        }

        public override void Initialise(ISignalScheduler scheduler, ICommandPoolFactory commandPoolFactory)
        {
            this.commandPoolFactory = commandPoolFactory;
            this.scheduler = scheduler;
            disposeList = new Queue<Command.Pool>(MaxCommands);
            commands = new List<Command.Pool>(MaxCommands);
            isSchedulingEnabled = scheduler != null;
        }

        protected int ContainsCommand<C>() where C : BaseCommand
        {
            var commandId = typeof(C);
            var index = -1;
            lock (commands)
            {
                for (var i = 0; i < commands.Count; ++i)
                {
                    if (commands[i].Type != commandId) continue;
                    index = i;
                    return index;
                }
            }
            return index;
        }

        public void MapCommandPool(Command.Pool pool)
        {
            lock (commands)
            {
                commands.Add(pool);
            }
        }

        public Command.Pool MapCommand<C>(int commandPoolSize = 1) where C : Command, new()
        {
            if (commands.Count >= MaxCommands)
                return null;
            
            var index = ContainsCommand<C>();
            Command.Pool pool;
            lock (commands)
            {
                if (index < 0)
                {
                    pool = commandPoolFactory.GetCommandPool<C>(commandPoolSize);
                    commands.Add(pool);
                }
                else
                {
                    pool = commands[index];
                }
            }
            return pool;
        }

        public void UnmapCommand<C>() where C : Command
        {
            var index = ContainsCommand<C>();
            if (index < 0) return;

            lock (commands)
            {
                var pool = commands[index];
                commands.RemoveAt(index);
                pool.Dispose();
            }
        }

        public void Subscribe(Action listener)
        {
            signalAction += listener;
        }

        public void Unsubscribe(Action listener)
        {
            signalAction -= listener;
        }

        internal override void ScheduledFire()
        {
            FireImmediate();
        }

        public void Fire()
        {
            if (isSchedulingEnabled)
            {
                scheduler.Enqueue(this);
            }
            else
            {
                FireImmediate();
            }
        }

        public void FireImmediate()
        {
            TriggerListeners();
            lock (commands)
            {
                TriggerCommands();
                MarkDisposedCommands();
            }
        }

        private void TriggerListeners()
        {
            if (signalAction == null) return;
            signalAction();
        }

        private void TriggerCommands()
        {
            for (var i = 0; i < commands.Count; ++i)
            {
                Command.Pool pool;
                lock (commands)
                {
                    pool = commands[i];
                }
                if (pool == null || !pool.HasExecutions) continue;

                Command c;
                lock (pool)
                {
                    c = pool.Spawn();
                }
                pool.TrackExecution();
                c.ExecuteCommand();
            }
        }

        private void MarkDisposedCommands()
        {
            for (var i = commands.Count - 1; i >= 0; --i)
            {
                var pool = commands[i];
                if (pool == null || pool.HasExecutions) continue;

                commands.RemoveAt(i);
                if (pool.IsManualDispose)
                {
                    lock (disposeList)
                    {
                        disposeList.Enqueue(pool);
                    }
                }
                else
                {
                    pool.Dispose();
                }
            }
        }

        public void DisposeUsedCommands()
        {
            lock (disposeList)
            {
                while (disposeList.Count > 0)
                {
                    var pool = disposeList.Dequeue();
                    pool.Dispose();
                }
            }
        }

        public override void Dispose()
        {
            signalAction = null;
            lock (commands)
            {
                for (var i = commands.Count - 1; i >= 0; --i)
                {
                    var pool = commands[i];
                    if (pool == null) continue;
                    commands.RemoveAt(i);
                    pool.Dispose();
                }
            }
            DisposeUsedCommands();
            scheduler = null;
        }
    }
}