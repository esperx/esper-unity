﻿using Esper.FSM;

namespace Esper.Signals
{
    public interface ISignal
    {
        void Initialise(ISignalScheduler scheduler = null, ICommandPoolFactory commandPoolFactory = null);
    }
}