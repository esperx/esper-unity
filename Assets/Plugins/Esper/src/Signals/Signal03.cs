﻿using System;
using System.Collections.Generic;
using Esper.Commands;
using Esper.FSM;
using UnityEngine;

namespace Esper.Signals
{
    public class Signal<T1, T2, T3> : BaseSignal
    {
        private ICommandPoolFactory commandPoolFactory;
        private ISignalScheduler scheduler;
        private Queue<Poolable> pooling;
        private Queue<Poolable> activeParams;
        private List<Command<T1, T2, T3>.Pool> commands;
        private Queue<Command<T1, T2, T3>.Pool> disposeList;
        private Action<T1, T2, T3> signalAction;
        private bool isSchedulingEnabled;

        protected virtual int MaxCommands
        {
            get { return 10; }
        }

        public override void Initialise(ISignalScheduler scheduler = null, ICommandPoolFactory commandPoolFactory = null)
        {
            if (scheduler != null)
            {
                this.scheduler = scheduler;
                SetupScheduling();
            }

            if (commandPoolFactory != null)
            {
                this.commandPoolFactory = commandPoolFactory;
                disposeList = new Queue<Command<T1, T2, T3>.Pool>(MaxCommands);
                commands = new List<Command<T1, T2, T3>.Pool>(MaxCommands); 
            }
        }

        private void SetupScheduling()
        {
            pooling = new Queue<Poolable>(MaxCommands);
            activeParams = new Queue<Poolable>(MaxCommands);

            for (var i = 0; i < MaxCommands; ++i)
            {
                pooling.Enqueue(new Poolable());
            }
            isSchedulingEnabled = true;
        }

        protected int ContainsCommand<C>() where C : BaseCommand
        {
            var commandId = typeof(C);
            var index = -1;
            lock (commands)
            {
                for (var i = 0; i < commands.Count; ++i)
                {
                    if (commands[i].Type != commandId) continue;
                    index = i;
                    return index;
                }
            }
            return index;
        }

        public Command<T1, T2, T3>.Pool MapCommand<C>(int commandPoolSize = 1) where C : Command<T1, T2, T3>, new()
        {
            if (commands.Count >= MaxCommands)
                return null;
            
            var index = ContainsCommand<C>();
            Command<T1, T2, T3>.Pool pool;
            lock (commands)
            {
                if (index < 0)
                {
                    pool = commandPoolFactory.GetCommandPool<C, T1, T2, T3>(commandPoolSize);
                    commands.Add(pool);
                }
                else
                {
                    pool = commands[index];
                }
            }
            return pool;
        }

        public void UnmapCommand<C>() where C : Command<T1, T2, T3>
        {
            var index = ContainsCommand<C>();
            lock (commands)
            {
                if (index >= 0)
                {
                    commands.RemoveAt(index);
                }
            }
        }

        public void Subscribe(Action<T1, T2, T3> listener)
        {
            signalAction += listener;
        }

        public void Unsubscribe(Action<T1, T2, T3> listener)
        {
            signalAction -= listener;
        }

        internal override void ScheduledFire()
        {
            if (activeParams.Count <= 0) return;

            Poolable argContainer;
            lock (activeParams)
            {
                argContainer = activeParams.Dequeue();
            }
            T1 arg1 = argContainer.Arg1;
        T2 arg2 = argContainer.Arg2;
        T3 arg3 = argContainer.Arg3;
            argContainer.Reset();
            lock (pooling)
            {
                pooling.Enqueue(argContainer);
            }
            FireImmediate(arg1, arg2, arg3);
        }

        public void Fire(T1 arg1 = default(T1), T2 arg2 = default(T2), T3 arg3 = default(T3))
        {
            if (isSchedulingEnabled)
            {
                Poolable argContainer;
                if (pooling.Count == 0)
                {
                    Debug.LogError("args not found");
                    return;
                }
                lock (pooling)
                {
                    argContainer = pooling.Dequeue();
                }
                argContainer.Arg1 = arg1;
        argContainer.Arg2 = arg2;
        argContainer.Arg3 = arg3;
                lock (activeParams)
                {
                    activeParams.Enqueue(argContainer);
                }
                scheduler.Enqueue(this);
            }
            else
            {
                FireImmediate(arg1, arg2, arg3);
            }
        }

        public void FireImmediate(T1 arg1 = default(T1), T2 arg2 = default(T2), T3 arg3 = default(T3))
        {
            TriggerListeners(arg1, arg2, arg3);
            lock (commands)
            {
                TriggerCommands(arg1, arg2, arg3);
                MarkDisposedCommands();
            }
        }

        private void TriggerListeners(T1 arg1, T2 arg2, T3 arg3)
        {
            if (signalAction == null) return;
            signalAction(arg1, arg2, arg3);
        }

        private void TriggerCommands(T1 arg1, T2 arg2, T3 arg3)
        {
            for (var i = 0; i < commands.Count; ++i)
            {
                Command<T1, T2, T3>.Pool pool;
                lock (commands)
                {
                    pool = commands[i];
                }
                if (pool == null || !pool.HasExecutions) continue;

                Command<T1, T2, T3> c;
                lock (pool)
                {
                    c = pool.Spawn();
                }
                pool.TrackExecution();
                c.ExecuteCommand(arg1, arg2, arg3);
            }
        }

        private void MarkDisposedCommands()
        {
            for (var i = commands.Count - 1; i >= 0; --i)
            {
                var pool = commands[i];
                if (pool == null || pool.HasExecutions) continue;

                commands.RemoveAt(i);

                if (pool.IsManualDispose)
                {
                    lock (disposeList)
                    {
                        disposeList.Enqueue(pool);
                    }
                }
                else
                {
                    pool.Dispose();
                }
            }
        }

        public void DisposeUsedCommands()
        {
            lock (disposeList)
            {
                while (disposeList.Count > 0)
                {
                    var pool = disposeList.Dequeue();
                    pool.Dispose();
                }
            }
        }

        public override void Dispose()
        {
            signalAction = null;
            lock (commands)
            {
                for (var i = commands.Count - 1; i >= 0; --i)
                {
                    var pool = commands[i];
                    if (pool == null) continue;
                    commands.RemoveAt(i);
                    pool.Dispose();
                }
            }
            DisposeUsedCommands();
            scheduler = null;
        }

        private class Poolable
        {
            public T1 Arg1 { get; set; }
        public T2 Arg2 { get; set; }
        public T3 Arg3 { get; set; }

            public void Reset()
            {
                this.Arg1 = default(T1);
        this.Arg2 = default(T2);
        this.Arg3 = default(T3);
            }
        }
    }
}
