﻿namespace Esper.Signals
{
    public interface ISignalScheduler
    {
        void Enqueue(BaseSignal sequenceObj);

        void Pause();

        void Resume();

        void Dispose();

        void Tick();

        void Initialize();
    }
}