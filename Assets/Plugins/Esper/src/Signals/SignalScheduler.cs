﻿using System.Collections.Generic;
using System.Diagnostics;
using Esper.Signals;

namespace Esper.Extensions.Basic
{
	public class SignalScheduler : ISignalScheduler
	{
		private const int MaxQueueSize = 100;
		private const int MaxQueueProcessingTime = 16;
		private Queue<BaseSignal> sequenceQueue;
		private static Stopwatch timer;
		private bool isPaused;

		public int QueueCount { get; private set; }

		public void Initialize()
		{
			timer = new Stopwatch();
			sequenceQueue = new Queue<BaseSignal>(MaxQueueSize);
			timer.Start();
		}

		public void Dispose()
		{
			sequenceQueue.Clear();
		}

		public void Enqueue(BaseSignal sequenceObj)
		{		
			sequenceQueue.Enqueue(sequenceObj);
			QueueCount++;
		}

		public void Tick()
		{
			timer.Reset();
				
			while (QueueCount > 0)
			{
				if (isPaused || timer.ElapsedMilliseconds > MaxQueueProcessingTime)
					return;

				lock (sequenceQueue)
				{
					var sequenceObj = sequenceQueue.Dequeue();
					QueueCount--;
					sequenceObj.ScheduledFire();
				}
			}
		}

		public void Pause()
		{
			isPaused = true;
		}

		public void Resume()
		{
			isPaused = false;
		}
	}
}