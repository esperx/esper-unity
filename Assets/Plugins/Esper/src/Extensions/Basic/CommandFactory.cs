﻿using Esper.Commands;

namespace Esper.Extensions.Basic
{
    public class CommandFactory : ICommandFactory
    {
        public C GetCommand<C>() where C : Command, new()
        {
            var command = new C();
            command.Initialize(this);
            return command;
        }
        
        public C GetCommand<C, T1>() where C : Command<T1>, new()
        {
            var command = new C();
            command.Initialize(this);
            return command;
        }
        
        public C GetCommand<C, T1, T2>() where C : Command<T1, T2>, new()
        {
            var command = new C();
            command.Initialize(this);
            return command;
        }
        
        public C GetCommand<C, T1, T2, T3>() where C : Command<T1, T2, T3>, new()
        {
            var command = new C();
            command.Initialize(this);
            return command;
        }
        
        public C GetCommand<C, T1, T2, T3, T4>() where C : Command<T1, T2, T3, T4>, new()
        {
            var command = new C();
            command.Initialize(this);
            return command;
        }
    }
}