﻿using System.Collections.Generic;
using Esper.FSM;
using Esper.Signals;

namespace Esper.Extensions.Basic
{
    public class SignalFactory : ISignalFactory
    {
        private Dictionary<string, ISignal> lookup;
        private ICommandPoolFactory commandPoolFactory;
        private ISignalScheduler signalScheduler;
        
        public SignalFactory(ISignalScheduler signalScheduler, ICommandPoolFactory commandPoolFactory, int maxSignals = 100)
        {
            lookup = new Dictionary<string, ISignal>(maxSignals);
            this.signalScheduler = signalScheduler;
            this.commandPoolFactory = commandPoolFactory;
        }
        
        public S GetSignal<S>() where S : ISignal, new()
        {
            var signalId = typeof(S).FullName;
            S signal;
            if (lookup.ContainsKey(signalId))
            {
                signal = (S)lookup[signalId];
            }
            else
            {
                signal = new S();
                signal.Initialise(signalScheduler, commandPoolFactory);
                lookup.Add(signalId, signal);
            }
            return signal;
        }
    }
}