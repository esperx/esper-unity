﻿using Esper.FSM;
using Esper.Services;
using Esper.Signals;
using UnityEngine;

namespace Esper.Extensions.Basic
{
    /**
     * A basic way of grabbing references to esper components.
     * Recommended method is to use dependency injection
     */
    
    public class BasicEsperRoot : EsperRoot
    {   
        protected override void PrepareApplication()
        {
            signalScheduler = new SignalScheduler();
            signalScheduler.Initialize();
            commandFactory = new CommandFactory();
            commandPoolFactory = new CommandPoolFactory(commandFactory);
            signalFactory = new SignalFactory(signalScheduler, commandPoolFactory);
            PrepareCoroutineService<CoroutineService>();
        }

        protected override void PrepareFSM()
        {
            fsmController = new FSMController();
            stateTransitionSignal = new StateTransitionSignal();
            stateTransitionSignal.Initialise(signalScheduler, commandPoolFactory);
        }
    }
}