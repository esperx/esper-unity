﻿using Esper.Commands;
using Esper.FSM;

namespace Esper.Extensions.Basic
{
    public class CommandPoolFactory : ICommandPoolFactory
    {
        private ICommandFactory commandFactory;

        public CommandPoolFactory(ICommandFactory commandFactory)
        {
            this.commandFactory = commandFactory;
        }
        
        public Command.Pool GetCommandPool<C>(int commandPoolSize) where C : Command, new()
        {
            var pool = new Command.Pool();
            pool.Initialize<C>(commandFactory, commandPoolSize);
            return pool;
        }

        public Command<T1>.Pool GetCommandPool<C, T1>(int commandPoolSize) where C : Command<T1>, new()
        {
            var pool = new Command<T1>.Pool();
            pool.Initialize<C>(commandFactory, commandPoolSize);
            return pool;
        }

        public Command<T1, T2>.Pool GetCommandPool<C, T1, T2>(int commandPoolSize) where C : Command<T1, T2>, new()
        {
            var pool = new Command<T1, T2>.Pool();
            pool.Initialize<C>(commandFactory, commandPoolSize);
            return pool;
        }

        public Command<T1, T2, T3>.Pool GetCommandPool<C, T1, T2, T3>(int commandPoolSize) where C : Command<T1, T2, T3>, new()
        {
            var pool = new Command<T1, T2, T3>.Pool();
            pool.Initialize<C>(commandFactory, commandPoolSize);
            return pool;
        }

        public Command<T1, T2, T3, T4>.Pool GetCommandPool<C, T1, T2, T3, T4>(int commandPoolSize) where C : Command<T1, T2, T3, T4>, new()
        {
            var pool = new Command<T1, T2, T3, T4>.Pool();
            pool.Initialize<C>(commandFactory, commandPoolSize);
            return pool;
        }
    }
}