﻿using System.Collections.Generic;

namespace Esper.Commands
{
	public class SequenceCommand<T1, T2> : Command<T1, T2>, ICommandListener
	{
		private List<Command<T1, T2>> commands;
		private int index;
		private T1 arg1;
        private T2 arg2;

		protected bool IsStopOnFailure
		{
			get;
			set;
		}

		public override void Initialize(ICommandFactory commandFactory)
		{
			base.Initialize(commandFactory);
			commands = new List<Command<T1, T2>>();
			QueueCommands();
		}

		internal override void ExecuteCommand(T1 arg1, T2 arg2)
		{	
			this.arg1 = arg1;
        this.arg2 = arg2;
			Execute(arg1, arg2);
		}

		protected void AddCommand<C>() where C : Command<T1, T2>, new()
		{
			commands.Add(CommandFactory.GetCommand<C, T1, T2>());
		}

		protected virtual void QueueCommands()
		{
		}

		public override void Execute(T1 arg1, T2 arg2)
		{
			if (index < commands.Count)
			{
				var command = commands[index];
				index++;
				command.ExecuteCommandWithListener(this, arg1, arg2);
			}
			else
			{
				CommandComplete(arg1, arg2);
			}
		}

		public void OnCommandComplete(bool success)
		{
			if (!success && IsStopOnFailure || index >= commands.Count)
			{
				CommandComplete(arg1, arg2);
			}
			else
			{
				Execute(arg1, arg2);
			}
		}

		protected override void CommandComplete(T1 arg1 = default(T1), T2 arg2 = default(T2))
		{
			index = 0;
			base.CommandComplete(arg1, arg2);
		}
	}
}
