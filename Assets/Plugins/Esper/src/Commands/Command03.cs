﻿using System;
using System.Collections.Generic;

namespace Esper.Commands
{
    public class Command<T1, T2, T3> : BaseCommand
    {
        private ICommandListener commandListener;

        protected ICommandFactory CommandFactory { get; set; }

        public Pool CommandPool { get; set; }

        protected bool IsSuccess { get; set; }

        public virtual void Initialize(ICommandFactory commandFactory)
        {
            CommandFactory = commandFactory;
        }

        internal virtual void ExecuteCommandWithListener(ICommandListener commandListener, T1 arg1, T2 arg2, T3 arg3)
        {
            IsSuccess = true;
            this.commandListener = commandListener;
            ExecuteCommand(arg1, arg2, arg3);
        }

        internal virtual void ExecuteCommand(T1 arg1, T2 arg2, T3 arg3)
        {
            Execute(arg1, arg2, arg3);
            CommandComplete(arg1, arg2, arg3);
        }

        public virtual void Execute(T1 arg1, T2 arg2, T3 arg3)
        {
        }

        protected virtual void CommandComplete(T1 arg1 = default(T1), T2 arg2 = default(T2), T3 arg3 = default(T3))
        {
            if (CommandPool != null)
            {
                lock (CommandPool)
                {
                    CommandPool.Despawn(this);
                }
            }
            if (commandListener != null)
                commandListener.OnCommandComplete(IsSuccess);
            commandListener = null;
        }

        public virtual void Destroy()
        {
            CommandFactory = null;
            CommandPool = null;
            commandListener = null;
        }

        public class Pool
        {
            private int maxSize;
            private int size;
            private Queue<Command<T1, T2, T3>> pool;
            private int maxExecutions = -1;
            private int currentExecutions;

            public int CurrentPoolSize
            {
                get { return pool.Count; }
            }

            public Type Type { get; private set; }

            public bool HasExecutions
            {
                get { return maxExecutions == -1 || currentExecutions < maxExecutions; }
            }

            internal bool IsManualDispose { get; private set; }

            public void Initialize<C>(ICommandFactory commandFactory, int maxSize) where C : Command<T1, T2, T3>, new()
            {
                this.maxSize = maxSize;
                Type = typeof(C);
                pool = new Queue<Command<T1, T2, T3>>(maxSize);
                for (var i = 0; i < maxSize; ++i)
                {
                    var command = commandFactory.GetCommand<C, T1, T2, T3>();
                    command.CommandPool = this;
                    command.Initialize(commandFactory);
                    pool.Enqueue(command);
                    size++;
                }
            }

            public Pool RestrictExecutionsTo(int executions)
            {
                if (executions <= 0) return this;
                currentExecutions = 0;
                maxExecutions = executions;
                return this;
            }

            public Pool ManualDispose()
            {
                IsManualDispose = true;
                return this;
            }

            public bool TrackExecution()
            {
                currentExecutions++;
                return HasExecutions;
            }

            public void Despawn(Command<T1, T2, T3> command)
            {
                if (size >= maxSize) return;

                size++;
                command.Reset();
                pool.Enqueue(command);
            }

            public Command<T1, T2, T3> Spawn()
            {
                if (size > 0)
                {
                    size--;
                    return pool.Dequeue();
                }
                #if DEVELOPMENT_BUILD
                else
                {   
					UnityEngine.Debug.LogWarning("CommandPool run out of objects");    
                }
                #endif
                return null;
            }

            public void Dispose()
            {
                while (pool.Count > 0)
                {
                    var c = pool.Dequeue();
                    c.CommandPool = null;
                    c.Destroy();
                }
            }
        }
    }
}
