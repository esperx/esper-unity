﻿using System;
using System.Collections.Generic;

namespace Esper.Commands
{
    public class Command : BaseCommand
    {
        private ICommandListener commandListener;

        protected ICommandFactory CommandFactory { get; set; }

        public Pool CommandPool { get; set; }

        protected bool IsSuccess { get; set; }

        public virtual void Initialize(ICommandFactory commandFactory)
        {
            CommandFactory = commandFactory;
        }

        internal void ExecuteCommandWithListener(ICommandListener commandListener)
        {
            IsSuccess = true;
            this.commandListener = commandListener;
            ExecuteCommand();
        }

        internal virtual void ExecuteCommand()
        {
            Execute();
            CommandComplete();
        }

        public virtual void Execute()
        {
        }

        protected virtual void CommandComplete()
        {
            if (CommandPool != null)
            {
                lock (CommandPool)
                {
                    CommandPool.Despawn(this);
                }
            }
            if (commandListener != null)
                commandListener.OnCommandComplete(IsSuccess);
            commandListener = null;
        }

        public virtual void Destroy()
        {
            CommandFactory = null;
            CommandPool = null;
            commandListener = null;
        }

        public class Pool
        {
            private int maxSize;
            private int size;
            private Queue<Command> pool;
            private int maxExecutions = -1;
            private int currentExecutions;

            public Type Type { get; private set; }

            internal bool IsManualDispose { get; private set; }

            public void Initialize<C>(ICommandFactory commandFactory, int maxSize) where C : Command, new()
            {
                this.maxSize = maxSize;
                Type = typeof(C);
                pool = new Queue<Command>(maxSize);
                for (var i = 0; i < maxSize; ++i)
                {
                    commandFactory.GetCommand<C>();
                    var command = commandFactory.GetCommand<C>();
                    command.CommandPool = this;
                    command.Initialize(commandFactory);
                    pool.Enqueue(command);
                    size++;
                }
            }

            public Pool RestrictExecutionsTo(int executions)
            {
                if (executions <= 0) return this;
                currentExecutions = 0;
                maxExecutions = executions;
                return this;
            }

            public Pool ManualDispose()
            {
                IsManualDispose = true;
                return this;
            }

            public void Despawn(Command command)
            {
                if (size >= maxSize) return;

                size++;
                command.Reset();
                pool.Enqueue(command);
            }

            public bool TrackExecution()
            {
                currentExecutions++;
                return maxExecutions > 0 && currentExecutions >= maxExecutions;
            }

            public bool HasExecutions
            {
                get { return maxExecutions == -1 || currentExecutions < maxExecutions; }
            }

            public Command Spawn()
            {
                if (size > 0)
                {
                    size--;
                    return pool.Dequeue();
                }
                #if DEVELOPMENT_BUILD
                else
                {
					UnityEngine.Debug.LogWarning("CommandPool run out of objects");    
                }
                #endif
                return null;
            }

            public void Dispose()
            {
                while (pool.Count > 0)
                {
                    var c = pool.Dequeue();
                    c.CommandPool = null;
                    c.Destroy();
                }
            }
        }
    }
}