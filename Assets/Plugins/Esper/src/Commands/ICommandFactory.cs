﻿namespace Esper.Commands
{
    public interface ICommandFactory
    {
        C GetCommand<C>() where C : Command, new();

        C GetCommand<C, T1>() where C : Command<T1>, new();
        
        C GetCommand<C, T1, T2>() where C : Command<T1, T2>, new();
        
        C GetCommand<C, T1, T2, T3>() where C : Command<T1, T2, T3>, new();
        
        C GetCommand<C, T1, T2, T3, T4>() where C : Command<T1, T2, T3, T4>, new();
    }
}