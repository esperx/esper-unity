﻿namespace Esper.Commands
{
	public class AsyncCommand<T1, T2, T3, T4> : Command<T1, T2, T3, T4>
	{
		internal override void ExecuteCommand(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{	
			Execute(arg1, arg2, arg3, arg4);
		}
	}
}
