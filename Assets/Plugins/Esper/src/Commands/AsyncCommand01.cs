﻿namespace Esper.Commands
{
	public class AsyncCommand<T1> : Command<T1>
	{
		internal override void ExecuteCommand(T1 arg1)
		{	
			Execute(arg1);
		}
	}
}