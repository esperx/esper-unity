﻿using System.Collections.Generic;

namespace Esper.Commands
{
	public class SequenceCommand : Command, ICommandListener
	{
		private List<Command> commands;
		private int index;

		protected bool IsStopOnFailure
		{
			get;
			set;
		}

		public override void Initialize(ICommandFactory commandFactory)
		{
			base.Initialize(commandFactory);
			commands = new List<Command>();
			QueueCommands();
		}

		protected void AddCommand<C>() where C : Command, new()
		{
			commands.Add(CommandFactory.GetCommand<C>());
		}

		protected virtual void QueueCommands()
		{
		}

		public override void Execute()
		{
			if (index < commands.Count)
			{
				var command = commands[index];
				index++;
				command.ExecuteCommandWithListener(this);
			}
			else
			{
				index = 0;
				CommandComplete();
			}
		}

		public void OnCommandComplete(bool success)
		{
			if ((!success && IsStopOnFailure) || index >= commands.Count)
			{
				CommandComplete();
			}
			else
			{
				index++;
				Execute();
			}
		}
		
		protected override void CommandComplete()
		{
			index = 0;
			base.CommandComplete();
		}

		public override void Destroy()
		{
			base.Destroy();
			for (var i = 0; i < commands.Count; ++i)
			{
				commands[i].Destroy();
			}
			commands.Clear();
		}
	}
}