﻿namespace Esper.Commands
{
	public class AsyncCommand<T1, T2> : Command<T1, T2>
	{
		internal override void ExecuteCommand(T1 arg1, T2 arg2)
		{	
			Execute(arg1, arg2);
		}
	}
}
