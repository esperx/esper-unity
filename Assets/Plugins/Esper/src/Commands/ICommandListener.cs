﻿namespace Esper.Commands
{
	public interface ICommandListener
	{
		void OnCommandComplete(bool success);
	}
}