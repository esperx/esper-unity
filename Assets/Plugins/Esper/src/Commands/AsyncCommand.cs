﻿namespace Esper.Commands
{
	public class AsyncCommand : Command
	{
		internal override void ExecuteCommand()
		{	
			Execute();
		}
	}
}