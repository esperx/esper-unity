﻿namespace Esper.Commands
{
	public class AsyncCommand<T1, T2, T3> : Command<T1, T2, T3>
	{
		internal override void ExecuteCommand(T1 arg1, T2 arg2, T3 arg3)
		{	
			Execute(arg1, arg2, arg3);
		}
	}
}
