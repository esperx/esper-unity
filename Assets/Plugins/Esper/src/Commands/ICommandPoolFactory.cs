﻿using Esper.Commands;

namespace Esper.FSM
{
    public interface ICommandPoolFactory
    {
        Command.Pool GetCommandPool<C>(int commandPoolSize) where C : Command, new();
        Command<T1>.Pool GetCommandPool<C, T1>(int commandPoolSize) where C : Command<T1>, new();
        Command<T1, T2>.Pool GetCommandPool<C, T1, T2>(int commandPoolSize) where C : Command<T1, T2>, new();
        Command<T1, T2, T3>.Pool GetCommandPool<C, T1, T2, T3>(int commandPoolSize) where C : Command<T1, T2, T3>, new();
        Command<T1, T2, T3, T4>.Pool GetCommandPool<C, T1, T2, T3, T4>(int commandPoolSize) where C : Command<T1, T2, T3, T4>, new();
    }
}