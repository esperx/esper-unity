﻿using UnityEngine;

namespace Esper.Utils
{
	public static class DebugExtensions
	{
		public static void Log(this IDebug service, string msg, params object[] args)
		{
			#if DEBUG
			Debug.Log(string.Format(msg, args));
			#endif
		}

		public static void LogWarning(this IDebug service, string msg, params object[] args)
		{
			#if DEBUG
			Debug.LogWarning(string.Format(msg, args));
			#endif
		}

		public static void LogError(this IDebug service, string msg, params object[] args)
		{
			#if DEBUG
			Debug.LogError(string.Format(msg, args));
			#endif
		}
	}
}