﻿using System.IO;

namespace Esper.Utils
{
	public class FileSystemUtils : IFileSystemUtils
	{
		public bool DirectoryExists(string path)
		{
			return Directory.Exists(path);
		}

		public bool FileExists(string path)
		{
			return File.Exists(path);
		}

		public void WriteAllText(string path, string contents)
		{
			File.WriteAllText(path, contents);
		}

		public void WriteAllBytes(string path, byte[] contents)
		{
			File.WriteAllBytes(path, contents);
		}

		public DirectoryInfo CreateDirectory(string path)
		{	
			return Directory.CreateDirectory(path);
		}

		public void EnsureDirectoryExists(string path)
		{
			if (!Directory.Exists(path))
			{
				CreateDirectory(path);
			}
		}

		public byte[] ReadAllBytes(string path)
		{
			return File.ReadAllBytes(path);
		}

		public string ReadAllText(string path)
		{
			return File.ReadAllText(path);
		}

		public void DeleteFile(string path)
		{
			if (FileExists(path))
			{
				File.Delete(path);
			}
		}
	}
}