﻿using System.IO;

namespace Esper.Utils
{
	public interface IFileSystemUtils
	{
		bool DirectoryExists(string path);

		bool FileExists(string path);

		void WriteAllText(string path, string contents);

		void WriteAllBytes(string path, byte[] contents);

		byte[] ReadAllBytes(string path);

		string ReadAllText(string path);

		DirectoryInfo CreateDirectory(string path);

		void EnsureDirectoryExists(string path);

		void DeleteFile(string path);
	}
}