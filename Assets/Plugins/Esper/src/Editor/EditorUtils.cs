﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Esper.Editor
{
	public class EditorUtils
	{
		public static string GetGameObjectPath(Transform transform)
		{
			var path = transform.name;
			while (transform.parent != null)
			{
				transform = transform.parent;
				path = transform.name + "." + path;
			}
			return path;
		}

		public static void CreateAsset<T>(string path) where T : ScriptableObject
		{
			if (File.Exists(path))
				return;
			var asset = ScriptableObject.CreateInstance<T>();
			var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path);

			AssetDatabase.CreateAsset(asset, assetPathAndName);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}


		public static void DirectoryCopy(string SourcePath, string DestinationPath, bool OverrideExistingFiles)
		{
			foreach (var dirPath in Directory.GetDirectories(SourcePath, "*", 
				SearchOption.AllDirectories))
				Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

			//Copy all the files & Replaces any files with the same name
			foreach (var newPath in Directory.GetFiles(SourcePath, "*.*", 
				SearchOption.AllDirectories))
			{
				if (OverrideExistingFiles || !File.Exists(newPath))
					File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), OverrideExistingFiles);
			}
		}

		public static void CreateDirectoryPath(string directory)
		{
			if (Directory.Exists(directory)) return;
			var path = directory.Split('/');
			var currentPath = string.Empty;
			for (var i = 0; i < path.Length; ++i)
			{
				var possibleDirectory = currentPath + path[i] + "/";
				if (possibleDirectory.Contains(".")) continue;
				if (!Directory.Exists(possibleDirectory))
				{
					Directory.CreateDirectory(possibleDirectory);
				}
				currentPath = possibleDirectory;
			}
		}
	}
}