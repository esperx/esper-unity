﻿using UnityEditor;
using UnityEngine;

namespace Esper.Editor
{
	public class EsperEditor : EditorWindow
	{
		private const string DirectoryKey = "esper.generated.content.directory";
		private const string AssetLabelFormat = "Asset Dir : {0}";
		private string assetLabel;
		private string assetDirectory;
		private string selectedDirectory;

		private EditorState editorState = EditorState.None;

		private FSMEditor fsmEditor;

		private enum EditorState
		{
			None,
			Initialising,
			Editing,
			Error
		}

		[MenuItem("Window/Esper/Config Editor")]
		private static void ShowEditor()
		{
			GetWindow<EsperEditor>("Esper Editor");
		}

		private void Update()
		{
			if (string.IsNullOrEmpty(assetDirectory))
			{
				if (!string.IsNullOrEmpty(EditorPrefs.GetString(DirectoryKey)))
				{
					assetDirectory = EditorPrefs.GetString(DirectoryKey);
					assetLabel = string.Format(AssetLabelFormat, assetDirectory);
					editorState = EditorState.Editing;
				}
				else
				{
					assetLabel = string.Format(AssetLabelFormat, "Not Selected");
					editorState = EditorState.Initialising;
				}
			}
			else
			{
				assetLabel = string.Format(AssetLabelFormat, assetDirectory);
				editorState = EditorState.Editing;
			}

			if (fsmEditor != null)
				fsmEditor.Update();
		}

		private void OnGUI()
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			EditorGUILayout.Space();
			EditorGUILayout.LabelField(assetLabel);
			ShowConfigFolderSelection();
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
			EditorGUILayout.Space();
			ShowMainPanel();
		}

		private void ShowMainPanel()
		{
			EditorGUI.BeginDisabledGroup(editorState != EditorState.Editing);

			if (fsmEditor == null)
			{
				fsmEditor = new FSMEditor();
			}
			ShowSubEditor();
			EditorGUI.EndDisabledGroup();
		}

		private void ShowSubEditor()
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			EditorGUILayout.Space();
			if (editorState == EditorState.Editing)
			{
				if (fsmEditor == null)
				{
					fsmEditor = new FSMEditor();
				}
				fsmEditor.Display(assetDirectory);
			}
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
		}

		private void ShowConfigFolderSelection()
		{
			if (!GUILayout.Button("Change Config Folder")) return;
			
			selectedDirectory = EditorUtility.OpenFolderPanel("Select Generated File Folder", Application.dataPath, "Generated Files Folder");

			if (string.IsNullOrEmpty(selectedDirectory)) return;
			
			selectedDirectory = selectedDirectory.Replace(Application.dataPath, "Assets");
			if(!selectedDirectory.Contains("Generated"))
			{
				selectedDirectory += "/Generated";
				EditorUtils.CreateDirectoryPath(selectedDirectory);
			}
			selectedDirectory = selectedDirectory + "/";
			assetDirectory = selectedDirectory;
			EditorPrefs.SetString(DirectoryKey, assetDirectory);
			editorState = EditorState.Editing;
		}
	}
}