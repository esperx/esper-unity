﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Esper.FSM;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace Esper.Editor
{
    public class FSMEditor
    {
        private const string JsonBackupName = "fsm-config-backup";
        private const string FSMConfigAsset = "FSMConfig.asset";
        private const string FSMAnimatorControllerAsset = "Editor/FSMController.controller";
        private const string FSMScriptingFolder = "Scripts/FSMController/";
        private const string FSMBackupFolder = "Editor/Backup/";
        private FSMConfig config;
        private AnimatorController controller;
        private EditorState editorState = EditorState.None;
        private Dictionary<string, StateSO> tmpStates;
        private List<AnimatorState> anyStates;
        private List<AnimatorState> animatorStates;
        private AnimatorState initialState;
        private AnimatorStateMachine root;

        private string fullControllerPath;
        private string fullAssetPath;
        private string fullScriptingPath;
        private string fullBackupPath;

        private List<StateSO> convertedStates;
        private List<string> requiredEvents;
        private string error;
        private bool addEnterState;
        private bool addExitState;
        private bool overrideExisting;

        private enum EditorState
        {
            None,
            Initialising,
            Editing,
            Saving,
            Error,
            Complete
        }

        public void Update()
        {
            if (config == null || controller == null || tmpStates == null)
            {
                editorState = EditorState.Initialising;
            }
        }

        public void Display(string rootGeneratedFolder)
        {
            fullControllerPath = rootGeneratedFolder + FSMAnimatorControllerAsset;
            fullAssetPath = rootGeneratedFolder + FSMConfigAsset;
            fullScriptingPath = rootGeneratedFolder + FSMScriptingFolder;
            fullBackupPath = rootGeneratedFolder + FSMBackupFolder;

            EditorUtils.CreateDirectoryPath(fullAssetPath);
            EditorUtils.CreateDirectoryPath(fullScriptingPath);
            EditorUtils.CreateDirectoryPath(fullControllerPath);
            EditorUtils.CreateDirectoryPath(fullBackupPath);

            if (editorState == EditorState.Initialising)
            {
                EditorGUILayout.HelpBox("Make sure assets have been created and have been loaded for editing.",
                    MessageType.Info);
                EditorGUILayout.Space();
                if (GUILayout.Button("Initialise FSMController Assets"))
                {
                    Initialise();
                    InitialiseConfig();
                    InitialiseController();
                    editorState = EditorState.Editing;
                }
            }
            else if (editorState == EditorState.Editing)
            {
                DisplayControllerInformation();
                EditorGUILayout.Space();
                DisplayFSMBehaviourPanel();
                EditorGUILayout.Space();
                DisplaySaveBackupPanel();
                EditorGUILayout.Space();
                CreateFSMFromJSON();
                EditorGUILayout.Space();
                DisplaySavePanel();
            }
            else if (editorState == EditorState.Saving)
            {
                EditorGUILayout.LabelField("Saving FSMController");
            }
            else if (editorState == EditorState.Complete)
            {
                EditorGUILayout.LabelField("FSMController Saved");
                if (GUILayout.Button("Edit FSMController"))
                {
                    editorState = EditorState.Editing;
                }
            }
            else if (editorState == EditorState.Error)
            {
                EditorGUILayout.HelpBox("Error creating error : " + error, MessageType.Error);
                if (GUILayout.Button("Try Again"))
                {
                    error = string.Empty;
                    editorState = EditorState.Editing;
                }
            }
        }

        private void Initialise()
        {
            animatorStates = new List<AnimatorState>();
            anyStates = new List<AnimatorState>();
            convertedStates = new List<StateSO>();
            tmpStates = new Dictionary<string, StateSO>();
            requiredEvents = new List<string>();
        }

        private void InitialiseConfig()
        {
            if (!File.Exists(fullAssetPath))
            {
                var asset = ScriptableObject.CreateInstance<FSMConfig>();
                var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(fullAssetPath);
                AssetDatabase.CreateAsset(asset, assetPathAndName);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
            
            config = AssetDatabase.LoadAssetAtPath<FSMConfig>(fullAssetPath);
            Debug.Log(config);
        }

        private void InitialiseController()
        {
            controller = !File.Exists(fullControllerPath)
                ? AnimatorController.CreateAnimatorControllerAtPath(fullControllerPath)
                : AssetDatabase.LoadAssetAtPath<AnimatorController>(fullControllerPath);
        }

        private void DisplaySaveBackupPanel()
        {
            EditorGUILayout.HelpBox("Save a backup of your FSMController to JSON", MessageType.Info);
            if (GUILayout.Button("Save Backup"))
            {
                EditorUtils.CreateDirectoryPath(fullBackupPath);
                PopulateTMPConfig();
                ConvertTMPStates();
                var jsonBackupPath = string.Format("{0}/{1}-{2:yyyy-MM-dd}.json", fullBackupPath, JsonBackupName, DateTime.UtcNow.Date);
                File.WriteAllText(jsonBackupPath, JsonUtility.ToJson(config));
                AssetDatabase.Refresh();
            }
        }

        private void DisplayControllerInformation()
        {
            EditorGUILayout.HelpBox(string.Format("Your state diagram can be found here: {0}", fullControllerPath),
                MessageType.Info);
        }

        private void DisplaySavePanel()
        {
            EditorGUILayout.HelpBox("Save all FSMController changes and create Signal classes.", MessageType.Info);
            
            if (!GUILayout.Button("Save FSMController")) return;
            
            editorState = EditorState.Saving;
            tmpStates.Clear();
            requiredEvents.Clear();
            if (controller.layers.Length != 1)
            {
                EditorGUILayout.HelpBox(
                    "FSMController Parser allows for one layer only. Please use sub state machines for sub tasks.",
                    MessageType.Info);
            }
            else
            {
                editorState = EditorState.Saving;
                try
                {
                    PopulateTMPConfig();
                    ConvertTMPStates();
                    DeleteCurrentGeneratedClasses();
                    BasicEditorUtils.CreateSignalClasses(fullScriptingPath, requiredEvents);
                    BasicEditorUtils.CreateSignalFactory(fullScriptingPath, requiredEvents);
                    CreateEventConsts();
                }
                catch (Exception e)
                {
                    error = e.Message;
                    editorState = EditorState.Error;
                }

                // save to config
                EditorUtility.SetDirty(config);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                editorState = EditorState.Complete;
            }
        }

        
        private void DisplayFSMBehaviourPanel()
        {
            EditorGUILayout.HelpBox(
                "Add FSMBehaviour scripts to each state.\nThe default config is no Signals fired.\nTo change this, open up the animator controller and select the state you want to add /remove Signals.\nIn the inspector, you should see the FSMBehaviour script and the event tick boxes.\nTick/Untick the relevant Signal(s).",
                MessageType.Info);
            addEnterState = EditorGUILayout.Toggle("Fire Signal on State Enter", addEnterState);
            addExitState = EditorGUILayout.Toggle("Fire Signal on State Exit", addExitState);
            overrideExisting = EditorGUILayout.Toggle("Override settings on states to match selection", overrideExisting);
            if (!GUILayout.Button("Enable State Signals")) return;

            PopulateTMPConfig();
            AddBehaviourScriptsToStates(addEnterState, addExitState, overrideExisting);
        }

        private void PopulateTMPConfig()
        {
            tmpStates.Clear();
            var baseLayer = controller.layers[0];
            root = baseLayer.stateMachine;
            initialState = root.defaultState;
            Debug.Log(root.name);
            FindAllStates(root);
        }

        private void FindAllStates(AnimatorStateMachine sm)
        {
            foreach (var transition in sm.anyStateTransitions)
            {
                if (transition.destinationState != null)
                {
                    anyStates.Add(transition.destinationState);
                }
                if (transition.destinationStateMachine != null &&
                    transition.destinationStateMachine.defaultState != null)
                {
                    anyStates.Add(transition.destinationStateMachine.defaultState);
                }
            }
            foreach (var state in sm.states)
            {
                var astate = state.state;
                var statePath = GetStatePath(astate);
                if (tmpStates.ContainsKey(statePath)) continue;

                var stateSO = new StateSO
                {
                    Position = state.position,
                    FullPath = statePath,
                    Id = statePath,
                    Name = astate.name,
                    Transitions = GetTransitions(astate)
                };
                GetBehaviours(astate, stateSO);
                tmpStates.Add(statePath, stateSO);
                animatorStates.Add(state.state);
            }
            foreach (var childSM in sm.stateMachines)
            {
                FindAllStates(childSM.stateMachine);
            }
        }

        private void GetBehaviours(AnimatorState astate, StateSO stateSO)
        {
            var behaviour =
                astate.behaviours.FirstOrDefault(x => x.GetType() == typeof(FSMBehaviour));
            if (behaviour == null) return;

            var fsmBehaviour = (FSMBehaviour) behaviour;

            var elements = stateSO.FullPath.Split('/');
            string eventName;
            switch (elements.Length)
            {
                case 0:
                    Debug.LogError("Error converting states. State path is not set");
                    return;
                case 1:
                    eventName = "OnRoot" + elements[0];
                    break;
                default:
                    eventName = "On" + elements[elements.Length - 2] + elements[elements.Length - 1];
                    break;
            }
            eventName = eventName.Replace(" ", string.Empty);

            if (fsmBehaviour.AddStateEnter)
            {
                requiredEvents.Add(eventName + "Enter");
                stateSO.OnStateEnter = eventName + "Enter";
            }
            if (fsmBehaviour.AddStateExit)
            {
                requiredEvents.Add(eventName + "Exit");
                stateSO.OnStateExit = eventName + "Exit";
            }
        }

        private void AddBehaviourScriptsToStates(bool addEnter, bool addExit, bool overrideExisting)
        {
            foreach (var state in animatorStates)
            {
                AddBehaviour(state, addEnter, addExit, overrideExisting);
            }
            EditorUtility.SetDirty(controller);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        private void AddBehaviour(AnimatorState state, bool addEnter, bool addExit, bool overrideExisting)
        {
            FSMBehaviour behaviour;
            if (state.behaviours.All(x => x.GetType() != typeof(FSMBehaviour)))
            {
                behaviour = state.AddStateMachineBehaviour<FSMBehaviour>();
                behaviour.AddStateEnter = addEnter;
                behaviour.AddStateExit = addExit;
            }
            else
            {
                behaviour = (FSMBehaviour) state.behaviours.First(x => x.GetType() == typeof(FSMBehaviour));
                if (!overrideExisting) return;
                behaviour.AddStateEnter = addEnter;
                behaviour.AddStateExit = addExit;
            }
        }

        private void ConvertTMPStates()
        {
            config.DefaultStateId = null;
            config.StateList = new List<StateSO>();
            convertedStates.Clear();
            foreach (var stateSO in tmpStates)
            {
                convertedStates.Add(stateSO.Value);
            }
            config.DefaultStateId = GetStatePath(initialState);
            config.StateList = convertedStates;
            EditorUtility.SetDirty(config);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        private string GetStatePath(AnimatorState animatorState)
        {
            var dynMethod = animatorState.GetType()
                .GetMethod("FindParent", BindingFlags.NonPublic | BindingFlags.Instance);
            var parent = dynMethod.Invoke(animatorState, new object[] {root}) as AnimatorStateMachine;
            return (parent == null)
                ? animatorState.name
                : string.Format("{0}/{1}", GetStateMachineName(parent), animatorState.name);
        }

        private string GetStateMachineName(AnimatorStateMachine stateMachine)
        {
            var statePath = stateMachine.name;
            var asm = stateMachine;
            while (true)
            {
                var dynMethod =
                    asm.GetType().GetMethod("FindParent", BindingFlags.NonPublic | BindingFlags.Instance);
                asm = dynMethod.Invoke(root, new object[] {asm}) as AnimatorStateMachine;
                if (asm == null)
                    break;
                statePath = string.Format("{0}/{1}", asm.name, statePath);
            }
            return statePath;
        }

        private List<string> GetTransitions(AnimatorState state)
        {
            var transitions = new List<string>();
            foreach (var transition in state.transitions)
            {
                if (transition.destinationState != null)
                {
                    transitions.Add(GetStatePath(transition.destinationState));
                }
                else if (transition.destinationStateMachine != null)
                {
                    transitions.Add(GetStatePath(transition.destinationStateMachine.defaultState));
                }
            }
            foreach (var anyState in anyStates)
            {
                var statePath = GetStatePath(anyState);
                if (!transitions.Contains(statePath))
                {
                    transitions.Add(statePath);
                }
            }
            return transitions;
        }

        private void DeleteCurrentGeneratedClasses()
        {
            var di = new DirectoryInfo(fullScriptingPath);

            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }
        }

        private void CreateEventConsts()
        {
            using (var eventFile =
                new StreamWriter(fullScriptingPath + "FSMTransitions.cs", false))
            {
                eventFile.WriteLine("namespace Esper.FSMController");
                eventFile.WriteLine("{");
                eventFile.WriteLine("\tpublic static class FSMTransitions");
                eventFile.WriteLine("\t{");
                foreach (var state in config.StateList)
                {
                    var mappingName = state.FullPath;
                    mappingName = mappingName.Replace("/", string.Empty);
                    mappingName = mappingName.Replace(" ", string.Empty);
                    eventFile.WriteLine("\t\tpublic const string {0} = \"{1}\";", mappingName, state.Id);
                }
                eventFile.WriteLine("\t}");
                eventFile.WriteLine("}");
            }
        }

        private void CreateFSMFromJSON()
        {
            if (!GUILayout.Button("Load from Json")) return;
            var jsonPath =
                EditorUtility.OpenFilePanelWithFilters("Select FSMController Json", Application.dataPath, new string[0]);
            if (string.IsNullOrEmpty(jsonPath)) return;
            // load file contents
            // attempt to convert json to config
            // create new animation controller and asset
            // apply fsm to controller
            // apply to config asset
            // if user is happy, and clicks ok, override existing assets
            // save
            var json = File.ReadAllText(jsonPath);
            Debug.Log(json + " " + jsonPath);
            if (File.Exists(fullBackupPath + "FSMConfig.asset"))
            {
                File.Delete(fullBackupPath + "FSMConfig.asset");
            }
            if (File.Exists(fullBackupPath + "FSMController.controller"))
            {
                File.Delete(fullBackupPath + "FSMController.controller");
            }
            var asset = ScriptableObject.CreateInstance<FSMConfig>();
            var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(fullBackupPath + "FSMConfig.asset");
            AssetDatabase.CreateAsset(asset, assetPathAndName);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            var bakcontroller =
                AnimatorController.CreateAnimatorControllerAtPath(fullBackupPath + "FSMController.controller");
            var tmpConfig = JsonUtility.FromJson<FSMData>(json);
            asset.DefaultStateId = tmpConfig.DefaultStateId;
            asset.StateList = tmpConfig.StateList;
            EditorUtility.SetDirty(asset);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            StateSOToAnimatorUtils.ConvertStateSO(tmpConfig, bakcontroller);
            EditorUtility.SetDirty(bakcontroller);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}