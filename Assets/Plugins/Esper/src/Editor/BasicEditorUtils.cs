﻿using System.Collections.Generic;
using System.IO;

namespace Esper.Editor
{
    public static class BasicEditorUtils
    {
        private static void CreateSignalClass(string fullScriptingPath, string eventName, string parentClass)
        {
            using (var eventFile =
                new StreamWriter(fullScriptingPath + eventName + ".cs", false))
            {
                eventFile.WriteLine("using Esper.FSM;");
                eventFile.WriteLine("");
                eventFile.WriteLine("namespace Esper.FSMController");
                eventFile.WriteLine("{");
                eventFile.WriteLine("\tpublic sealed class " + eventName + " : " + parentClass);
                eventFile.WriteLine("\t{");
                eventFile.WriteLine("\t}");
                eventFile.WriteLine("}");
            }
        }

        public static void CreateSignalFactory(string fullScriptingPath, List<string> fsmSignals)
        {
            EditorUtils.CreateDirectoryPath(fullScriptingPath);
            var signalFactoryPath = fullScriptingPath + "FSMSignalFactory.cs";
            if (File.Exists(signalFactoryPath)) File.Delete(signalFactoryPath);
            using (var factoryFile =
                new StreamWriter(signalFactoryPath, false))
            {
                factoryFile.WriteLine("using Esper.FSM;");
                factoryFile.WriteLine("using Esper.Signals;");
                factoryFile.WriteLine("");
                factoryFile.WriteLine("namespace Esper.FSMController");
                factoryFile.WriteLine("{");
                factoryFile.WriteLine("\tpublic class FSMSignalFactory : IFSMSignalFactory");
                factoryFile.WriteLine("\t{");
                factoryFile.WriteLine("\t\tprivate ISignalFactory signalFactory;");
                foreach (var signalClassName in fsmSignals)
                {
                    var signalFieldName = char.ToLowerInvariant(signalClassName[0]) + signalClassName.Substring(1);
                    factoryFile.WriteLine("\t\tprivate {0} {1};", signalClassName, signalFieldName);
                }

                factoryFile.WriteLine("");
                factoryFile.WriteLine("\t\tpublic FSMSignalFactory(ISignalFactory signalFactory)");
                factoryFile.WriteLine("\t\t{");
                factoryFile.WriteLine("\t\t\tthis.signalFactory = signalFactory;");
                factoryFile.WriteLine("\t\t}");
                factoryFile.WriteLine("");

                WritePrewarmSignalsFunction(factoryFile, fsmSignals);
                WriteGetSignalFunction(factoryFile, fsmSignals);
                WriteDisposeFunction(factoryFile, fsmSignals);

                factoryFile.WriteLine("\t}");
                factoryFile.WriteLine("}");
            }
        }

        public static void CreateSignalClasses(string fullScriptingPath, List<string> requiredEvents)
        {
            for (var i = 0; i < requiredEvents.Count; ++i)
            {
                var signalClassName = requiredEvents[i];
                var parentClass = signalClassName.Contains("Enter") ? "StateEnterSignal" : "StateExitSignal";
                CreateSignalClass(fullScriptingPath, signalClassName, parentClass);
            }
        }

        private static void WritePrewarmSignalsFunction(StreamWriter factoryFile, List<string> requiredEvents)
        {
            factoryFile.WriteLine("\t\tpublic void PrewarmSignals()");
            factoryFile.WriteLine("\t\t{");
            for (var i = 0; i < requiredEvents.Count; ++i)
            {
                var signalClassName = requiredEvents[i];
                var signalFieldName = char.ToLowerInvariant(signalClassName[0]) + signalClassName.Substring(1);

                factoryFile.WriteLine("\t\t\tif ({1} == null) {1} = signalFactory.GetSignal<{0}>();", signalClassName,
                    signalFieldName);
            }

            factoryFile.WriteLine("\t\t}");
        }

        private static void WriteGetSignalFunction(StreamWriter factoryFile, List<string> requiredEvents)
        {
            factoryFile.WriteLine("\t\tpublic StateSignal GetSignal(string signalId)");
            factoryFile.WriteLine("\t\t{");
            factoryFile.WriteLine("\t\t\tswitch(signalId)");
            factoryFile.WriteLine("\t\t\t{");
            for (var i = 0; i < requiredEvents.Count; ++i)
            {
                var signalClassName = requiredEvents[i];
                var signalFieldName = char.ToLowerInvariant(signalClassName[0]) + signalClassName.Substring(1);

                factoryFile.WriteLine("\t\t\t\tcase\"{0}\": return {1} ?? ({1} = signalFactory.GetSignal<{0}>());",
                    signalClassName, signalFieldName);
            }
            factoryFile.WriteLine("\t\t\t}");
            factoryFile.WriteLine("\t\t\treturn null;");
            factoryFile.WriteLine("\t\t}");
        }

        private static void WriteDisposeFunction(StreamWriter factoryFile, List<string> requiredEvents)
        {
            factoryFile.WriteLine("\t\tpublic void Dispose()");
            factoryFile.WriteLine("\t\t{");
            for (var i = 0; i < requiredEvents.Count; ++i)
            {
                var signalClassName = requiredEvents[i];
                var signalFieldName = char.ToLowerInvariant(signalClassName[0]) + signalClassName.Substring(1);
                factoryFile.WriteLine("\t\t\t{0}.Dispose();", signalFieldName);
            }

            factoryFile.WriteLine("\t\t}");
        }
    }
}