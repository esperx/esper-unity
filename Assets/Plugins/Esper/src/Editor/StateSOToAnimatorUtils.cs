﻿using System.Collections.Generic;
using Esper.FSM;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace Esper.Editor
{
	public class StateSOToAnimatorUtils
	{
		public static void ConvertStateSO(FSMData data, AnimatorController animController)
		{
			List<StateSO> states = data.StateList;
			var statesDict = new Dictionary<string, AnimatorState>();
			var allStateMachines = new Dictionary<string, AnimatorStateMachine>();
			AnimatorStateMachine root = animController.layers[0].stateMachine;
			root.name = "Base Layer";

			allStateMachines.Add("/" + root.name, root);

			foreach (StateSO stateso in states)
			{
				string fullPath = stateso.FullPath;
				string[] components = fullPath.Split('/');
				string path = "";
				for (int i = 0; i < components.Length; ++i)
				{
					// Add all state machines

					if (i < components.Length - 1)
					{
						string componentName = components[i];
						string currentPath = path + "/" + components[i];
						if (i > 0) // don't create new base layer
						{
							if (!allStateMachines.ContainsKey(currentPath))
							{
								AnimatorStateMachine parent = allStateMachines[path];
								var aStateMachine = parent.AddStateMachine(componentName);
								var stateMachine = new ChildAnimatorStateMachine();
								stateMachine.stateMachine = aStateMachine;
								allStateMachines.Add(currentPath, aStateMachine);
							}
						}
						path = currentPath;
					}
					else
					{
						Debug.Log(path);
						AnimatorStateMachine sm = allStateMachines[path];
						AnimatorState animatorState = sm.AddState(stateso.Name);
						var childAnimatorState = new ChildAnimatorState();
						animatorState.name = stateso.Name;
						childAnimatorState.state = animatorState;
						childAnimatorState.position = stateso.Position;
						statesDict.Add(stateso.FullPath, animatorState);
					}
				}
			}

			root.defaultState = statesDict[data.DefaultStateId];
			EditorUtility.SetDirty(animController);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();

			foreach (StateSO stateso in states)
			{
				AnimatorState aState = statesDict[stateso.FullPath];
				aState.behaviours = GetBehaviours(stateso);
			}

			foreach (StateSO stateso in states)
			{
				AnimatorState aState = statesDict[stateso.FullPath];
				aState.transitions = GetTransitions(stateso, statesDict);
			}


			EditorUtility.SetDirty(animController);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		private static StateMachineBehaviour[] GetBehaviours(StateSO stateSO)
		{
			string onstateEnter = stateSO.OnStateEnter;
			string onstateExit = stateSO.OnStateExit;

			var behaviour = ScriptableObject.CreateInstance<FSMBehaviour>();

			if (!string.IsNullOrEmpty(onstateEnter))
				behaviour.AddStateEnter = true;
			if (!string.IsNullOrEmpty(onstateExit))
				behaviour.AddStateExit = true;

			return new StateMachineBehaviour[]{ behaviour };
		}

		private static AnimatorStateTransition[] GetTransitions(StateSO state, Dictionary<string, AnimatorState> statesDict)
		{
			var transitions = new List<AnimatorStateTransition>();
			foreach (string transition in state.Transitions)
			{
				var trans = new AnimatorStateTransition();
				trans.destinationState = statesDict[transition];
				transitions.Add(trans);
			}
			return transitions.ToArray();
		}
	}
}