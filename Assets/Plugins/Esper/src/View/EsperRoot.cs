﻿using Esper.Commands;
using Esper.FSM;
using Esper.Services;
using Esper.Signals;
using UnityEngine;

namespace Esper.Extensions.Basic
{
    public class EsperRoot : MonoBehaviour
    {
        protected IFSMController fsmController;
        protected StateTransitionSignal stateTransitionSignal;
        protected ISignalFactory signalFactory;
        
        public IFSMSignalFactory fsmSignalFactory { get; protected set; }
        
        public ISignalScheduler signalScheduler { get; protected set; }
        
        public ICommandPoolFactory commandPoolFactory { get; protected set; }
        
        public ICommandFactory commandFactory { get; protected set; }
        
        public ICoroutineService CoroutineService { get; protected set; }

        public bool IsInitialised { get; protected set; }

        protected void PrepareCoroutineService<C>() where C : MonoBehaviour, ICoroutineService
        {
            var go = new GameObject("CoroutineService");
            DontDestroyOnLoad(go);
            CoroutineService = go.AddComponent<C>();
        }

        protected virtual void LateUpdate()
        {
            if (signalScheduler != null) signalScheduler.Tick();
        }

        protected virtual void OnDestroy()
        {
            fsmSignalFactory.Dispose();
            signalScheduler.Dispose();
        }
        
        protected virtual void Initialise()
        {
            if (IsInitialised) return;
            PrepareApplication();
            PrepareFSM();
            PrepareSignals();
            PrepareCommands();
            StartApplication();
            IsInitialised = true;
        }

        protected virtual void PrepareApplication()
        {
            
        }
        
        protected virtual void PrepareSignals()
        {
            
        }
        
        protected virtual void PrepareCommands()
        {
            
        }
        
        protected virtual void PrepareFSM()
        {
        }

        protected virtual void StartApplication()
        {
            
        }

        public void TriggerState(string stateId, object passthrough = null)
        {
            stateTransitionSignal.Fire(stateId, passthrough);
        }
        
        public S Signal<S>() where S : ISignal, new()
        {
            return signalFactory.GetSignal<S>();
        }
    }
}